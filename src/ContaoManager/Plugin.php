<?php

/**
 * @copyright  Sven Rhinow 2017 <https://www.sr-tag.de>
 * @author     Sven Rhinow
 * @package    LedproductsBundle
 * @license    LGPL-3.0+
 * @see	       https://github.com/srhinow/ledproducts-bundle
 *
 */

namespace Srhinow\LedproductsBundle\ContaoManager;

use Contao\CoreBundle\ContaoCoreBundle;
use Contao\ManagerPlugin\Bundle\Config\BundleConfig;
use Contao\ManagerPlugin\Bundle\BundlePluginInterface;
use Contao\ManagerPlugin\Bundle\Parser\ParserInterface;

use Srhinow\LedproductsBundle\SrhinowLedproductsBundle;

/**
 * Plugin for the Contao Manager.
 *
 * @author Sven Rhinow
 */
class Plugin implements BundlePluginInterface
{
    /**
     * {@inheritdoc}
     */
    public function getBundles(ParserInterface $parser)
    {
        return [
            BundleConfig::create(SrhinowLedproductsBundle::class)
                ->setLoadAfter([ContaoCoreBundle::class])
        ];
    }
}
