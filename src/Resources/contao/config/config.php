<?php
/**
 * Created by ledproducts-bundle.
 * Developer: Sven Rhinow (sven@sr-tag.de)
 * Date: 07.12.17
 */

$GLOBALS['LED']['MODULE_RELPATH'] = "system/modules/ledproducts-bundle";
$GLOBALS['LED']['PROPERTIES']['ID'] = 1;


/**
 * -------------------------------------------------------------------------
 * BACK END MODULES
 * -------------------------------------------------------------------------
 */
array_insert($GLOBALS['BE_MOD'], 1, array(
        'led' => array
        (
            'led_products' => array (
                'tables' => array('tl_led_series','tl_led_filter','tl_led_categories','tl_led_serie_categories','tl_led_serie_articles'),
            ),
//            'led_properties' => array (
//                'tables' => array('tl_led_properties'),
//                'callback' => 'ModuleLedProperties',
//            )
        )
    )
);

/**
 * -------------------------------------------------------------------------
 * Frontend modules
 * -------------------------------------------------------------------------
 */
$GLOBALS['FE_MOD']['led_products_fe'] = array
(
    'led_category_list' => 'Srhinow\LedproductsBundle\ModuleLedCategoryList',
    'led_serie_list' => 'Srhinow\LedproductsBundle\ModuleLedSerieList',
    'led_serie_details' => 'Srhinow\LedproductsBundle\ModuleLedSerieDetails',
    'led_serie_articles' => 'Srhinow\LedproductsBundle\ModuleLedSerieArticlesList',
    'led_serie_article_details' => 'Srhinow\LedproductsBundle\ModuleLedSerieArticleDetails',
);

/**
 * -------------------------------------------------------------------------
 * Models
 * -------------------------------------------------------------------------
 */
$GLOBALS['TL_MODELS']['tl_led_categories'] = Srhinow\LedproductsBundle\LedCategoriesModel::class;
$GLOBALS['TL_MODELS']['tl_led_filter'] = Srhinow\LedproductsBundle\LedFilterModel::class;
$GLOBALS['TL_MODELS']['tl_led_series'] = Srhinow\LedproductsBundle\LedSeriesModel::class;
$GLOBALS['TL_MODELS']['tl_led_serie_articles'] = Srhinow\LedproductsBundle\LedSerieArticlesModel::class;
$GLOBALS['TL_MODELS']['tl_led_serie_categories'] = Srhinow\LedproductsBundle\LedSerieCategoriesModel::class;

/**
 * -------------------------------------------------------------------------
 * Hooks
 * -------------------------------------------------------------------------
 */
$GLOBALS['TL_HOOKS']['generateBreadcrumb'][] = array('Srhinow\LedproductsBundle\LedHooks', 'LedGenerateBreadcrumb');
$GLOBALS['TL_HOOKS']['replaceInsertTags'][] = array('Srhinow\LedproductsBundle\LedHooks', 'ledReplaceInsertTags');