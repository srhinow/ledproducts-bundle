<?php

/**
 * PHP version 7
 * @copyright  Sven Rhinow Webentwicklung 2018 <http://www.sr-tag.de>
 * @author     Sven Rhinow
 * @package    ledproducts-bundle
 * @license	   LGPL +3.0
 * @filesource
 */

namespace Srhinow\LedproductsBundle;

use Contao\Pagination;

/**
 * Class ModuleLedCategoryList
 */
class ModuleLedSerieList extends ModuleLed
{
    /**
     * Template
     * @var string
     */
    protected $strTemplate = 'led_serie_list';


    /**
     * Target pages
     * @var array
     */
    protected $arrTargets = array();


    /**
     * Display a wildcard in the back end
     * @return string
     */
    public function generate()
    {
        if (TL_MODE == 'BE')
        {
            $objTemplate = new \BackendTemplate('be_wildcard');

            $objTemplate->wildcard = '### LED SERIE LIST ###';

            $objTemplate->title = $this->headline;
            $objTemplate->id = $this->id;
            $objTemplate->link = $this->name;
            $objTemplate->href = 'contao/main.php?do=modules&amp;act=edit&amp;id=' . $this->id;

            return $objTemplate->parse();
        }

        // Set the item from the auto_item parameter
        if (!isset($_GET['category']) && $GLOBALS['TL_CONFIG']['useAutoItem'] && isset($_GET['auto_item']))
        {
            \Input::setGet('category', \Input::get('auto_item'));
        }
        // Set category if in Frontend-Modul was set
        elseif(!empty($this->setCategory))
        {
            \Input::setGet('category', $this->setCategory);
        }


        // Do not index or cache the page if no category item has been specified
        if (!\Input::get('category'))
        {
            global $objPage;
            $objPage->noSearch = 1;
            $objPage->cache = 0;
            return '';
        }

        // Fallback template
        if (strlen($this->fe_led_template)) $this->strTemplate = $this->fe_led_template;

        return parent::generate();
    }


    /**
     * Generate module
     */
    protected function compile()
    {
        /** @var \PageModel $objPage */
        global $objPage;

        $categoryObj = LedCategoriesModel::findCategoryByIdOrAlias(\Input::get('category'));

        if (null === $categoryObj)
        {
            /** @var \PageError404 $objHandler */
            $objHandler = new $GLOBALS['TL_PTY']['error_404']();
            $objHandler->generate($objPage->id);
        }

        $offset = 0;
        $limit = null;
        $itemsArray = $filterArr = array();

        // Maximum number of items
        if ($this->fe_led_numberOfItems > 0)
        {
            $limit = $this->fe_led_numberOfItems;
        }

        $searchWhereArr[] = "`published` = 1";

        // Get the total number of items
        $intTotal = LedSeriesModel::countPublishedByPid($categoryObj->id);

        // Filter anwenden um die Gesamtanzahl zuermitteln
        if((int) $intTotal > 0)
        {
            $total = $intTotal - $offset;

            // Split the results
            if ($this->perPage > 0 && (!isset($limit) || $this->fe_led_numberOfItems > $this->perPage))
            {

                // Adjust the overall limit
                if (isset($limit))
                {
                    $total = min($limit, $total);
                }

                // Get the current page
                $id = 'page_n' . $this->id;
                $page = \Input::get($id) ?: 1;

                // Do not index or cache the page if the page number is outside the range
                if ($page < 1 || $page > max(ceil($total/$this->perPage), 1))
                {
                    global $objPage;
                    $objPage->noSearch = 1;
                    $objPage->cache = 0;

                    // Send a 404 header
                    header('HTTP/1.1 404 Not Found');
                    return;
                }

                // Set limit and offset
                $limit = $this->perPage;
                $offset = (max($page, 1) - 1) * $this->perPage;

                // Overall limit
                if ($offset + $limit > $total)
                {
                    $limit = $total - $offset;
                }

                // Add the pagination menu
                $objPagination = new Pagination($total, $this->perPage);
                $this->Template->pagination = $objPagination->generate("\n  ");
            }

            // Get the items
            if (isset($limit))
            {
                $itemsObj = LedSeriesModel::findPublishedByPid($categoryObj->id, $limit, $offset, $searchWhereArr, array('order' => "sorting, name DESC") );
            }
            else
            {
                $itemsObj = LedSeriesModel::findPublishedByPid($categoryObj->id, 0, $offset, $searchWhereArr, array('order' => "sorting, name DESC"));
            }

            $count = -1;


            if(is_object($itemsObj) )
                while($itemsObj->next())
                {
                    //row - Class
                    $class = 'row_' . ++$count . (($count == 0) ? ' row_first' : '') . (($count >= ($limit - 1)) ? ' row_last' : '') . ((($count % 2) == 0) ? ' even' : ' odd');

                    //Filter holen
                    if($itemsObj->filter > 0)
                    {
                        $filterObj = LedFilterModel::findById($itemsObj->filter);

                        if(is_object($filterObj)){

                            //Class ergaenzen
                            $class .= ' '.$filterObj->alias;

                            //Filter-Array-Item setzen wenn nicht schon vorhanden
                            $filterArr[$filterObj->id] = array(
                                'id' => $filterObj->id,
                                'name' => $filterObj->name,
                                'alias' => $filterObj->alias
                            );
                        }
                    }

                    // Add the article image as enclosure
                    $image = '';
                    if($itemsObj->addImage) {
                        $objFile = \FilesModel::findByUuid($itemsObj->image);
                        if ($objFile !== null)
                        {
                            $image = $objFile->path;
                        }
                    }

                    // Add the article image as enclosure
                    $teaserImage = '';
                    if($itemsObj->addTeaserImage) {
                        $objFile = \FilesModel::findByUuid($itemsObj->teaserImage);
                        if ($objFile !== null)
                        {
                            $teaserImage = $objFile->path;
                        }
                    }
                    
                    //Products-Url
                    $productsUrl = false;
                    if($this->jumpTo)
                    {
                        $objProductsPage = \PageModel::findByPk($this->jumpTo);
                        $productsUrl = ampersand( $objProductsPage->getFrontendUrl('/'.$itemsObj->alias) );
                    }

                    $itemsArray[] = array(
                        'name' => $itemsObj->name,
                        'class' => $class,
                        'alias' => $itemsObj->alias,
                        'desription' => $itemsObj->desription,
                        'image' => $image,
                        'teaserImage' => $teaserImage,
                        'alt' => $itemsObj->alt,
                        'productsUrl' => $productsUrl,

                    );
                }
        }

        $this->Template->headline = $this->headline;
        $this->Template->items = $itemsArray;
        $this->Template->filterArr = $filterArr;
        $this->Template->messages = ''; // Backwards compatibility
    }

}
