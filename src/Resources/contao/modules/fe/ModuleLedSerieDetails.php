<?php

/**
 * PHP version 7
 * @copyright  Sven Rhinow Webentwicklung 2018 <http://www.sr-tag.de>
 * @author     Sven Rhinow
 * @package    ledproducts-bundle
 * @license	   LGPL +3.0
 * @filesource
 */

namespace Srhinow\LedproductsBundle;

/**
 * Class ModuleLedSerieDetails
 */
class ModuleLedSerieDetails extends ModuleLed
{
    /**
     * Template
     * @var string
     */
    protected $strTemplate = 'led_serie_details';


    /**
     * Target pages
     * @var array
     */
    protected $arrTargets = array();

    protected $objSerie;

    /**
     * Display a wildcard in the back end
     * @return string
     */
    public function generate()
    {
        if (TL_MODE == 'BE')
        {
            $objTemplate = new \BackendTemplate('be_wildcard');

            $objTemplate->wildcard = '### LED SERIE DETAILS ###';

            $objTemplate->title = $this->headline;
            $objTemplate->id = $this->id;
            $objTemplate->link = $this->name;
            $objTemplate->href = 'contao/main.php?do=modules&amp;act=edit&amp;id=' . $this->id;

            return $objTemplate->parse();
        }

        // Set the item from the auto_item parameter
        if (!isset($_GET['serie']) && $GLOBALS['TL_CONFIG']['useAutoItem'] && isset($_GET['auto_item']))
        {
            \Input::setGet('serie', \Input::get('auto_item'));
        }

        // Do not index or cache the page if no category item has been specified
        if (!\Input::get('serie'))
        {
            global $objPage;
            $objPage->noSearch = 1;
            $objPage->cache = 0;
            return '';
        }

        // Fallback template
        if (strlen($this->fe_led_template)) $this->strTemplate = $this->fe_led_template;

        $this->objSerie = LedSeriesModel::findSerieByIdOrAlias(\Input::get('serie'));

        //falls spezielles Template aus der aktuellen Serien-Einstellungen gesetzt ist, wird es hier zugewiesen
        if (strlen($this->objSerie->serieDetailTpl)> 0 ) $this->strTemplate = $this->objSerie->serieDetailTpl;

        return parent::generate();
    }


    /**
     * Generate module
     */
    protected function compile()
    {
        /** @var \PageModel $objPage */
        global $objPage;
//        dump($this->objSerie); die();

        if ($this->objSerie === null)
        {
            // Do not index or cache the page
            $objPage->noSearch = 1;
            $objPage->cache = 0;

            // Send a 404 header
            header('HTTP/1.1 404 Not Found');
            $this->Template->messages = '<p class="error">' . sprintf($GLOBALS['TL_LANG']['MSC']['invalidPage'], \Input::get('items')) . '</p>';
            return;
        }

        $objFile = \FilesModel::findByUuid($this->objSerie->image);
        $image = false;

        if ($objFile !== null)
        {
            $image = $objFile->path;
        }

        // Get the file entries from the database
        $this->objSerie->multiSRC = \StringUtil::deserialize($this->objSerie->multiSRC);
        $objFiles = \FilesModel::findMultipleByUuids($this->objSerie->multiSRC);
        $arrFiles = [];
        if(null !== $objFiles){
            while($objFiles->next()) {

                $objFile = new \File($objFiles->path);
                $arrFiles[$objFiles->path] =
                    [
                        'id'         => $objFiles->id,
                        'uuid'       => $objFiles->uuid,
                        'name'       => $objFile->basename,
                        'singleSRC'  => $objFiles->path,
                        'title'      => \StringUtil::specialchars($objFile->basename),
                        'filesModel' => $objFiles->current()
                    ];

                $order = \StringUtil::deserialize($this->objSerie->orderSRC);

                if (!empty($order) && \is_array($order))
                {
                    // Remove all values
                    $arrOrder = array_map(function () {}, array_flip($order));

                    // Move the matching elements to their position in $arrOrder
                    foreach ($arrFiles as $k=>$v)
                    {
                        if (\array_key_exists($v['uuid'], $arrOrder))
                        {
                            $arrOrder[$v['uuid']] = $v;
                            unset($arrFiles[$k]);
                        }
                    }

                    // Append the left-over images at the end
                    if (!empty($arrFiles))
                    {
                        $arrOrder = array_merge($arrOrder, array_values($arrFiles));
                    }

                    // Remove empty (unreplaced) entries
                    $arrFiles = array_values(array_filter($arrOrder));
                    unset($arrOrder);
                }
            }


        }

        $itemArray = array(
            'id' => $this->objSerie->id,
            'name' => $this->objSerie->name,
            'alias' => $this->objSerie->alias,
            'teaser' => $this->objSerie->teaser,
            'features' => $this->objSerie->features,
            'text' => strlen($this->objSerie->description)?$this->objSerie->description:$this->objSerie->teaser,
            'desription' => $this->objSerie->description,
            'addImage' =>$this->objSerie->addImage,
            'clickForBigger' =>$this->objSerie->clickForBigger,
            'image' => $image,
            'addGallery' =>$this->objSerie->addGallery,
            'gallery' =>$arrFiles,
            'alt' => $this->objSerie->alt
        );

        $this->Template->headline = $this->headline;
        $this->Template->item = $itemArray;

        $objPage->pageTitle = $this->objSerie->name;
        //SEO-Werte setzen
        if(strlen($this->objSerie->seo_title) > 0) $objPage->pageTitle = strip_tags(strip_insert_tags($this->objSerie->seo_title));
        if(strlen($this->objSerie->seo_keywords) > 0) $GLOBALS['TL_KEYWORDS'] .= strip_tags(strip_insert_tags($this->objSerie->seo_keywords));
        if(strlen($this->objSerie->seo_description) > 0) $objPage->description = strip_tags(strip_insert_tags($this->objSerie->seo_description));

//        dump($this->strTemplate); die();
    }

}
