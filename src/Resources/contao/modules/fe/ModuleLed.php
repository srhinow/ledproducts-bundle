<?php

/**
 * for Contao Open Source CMS
 *
 * Copyright (c) 2018 Sven Rhinow
 *
 * @package ledproducts-bundle
 * @license http://www.gnu.org/licenses/lgpl-3.0.html LGPL
 */

namespace Srhinow\LedproductsBundle;


use Contao\Environment;
use Contao\File;
use Contao\FilesModel;
use Contao\Image;
use Contao\StringUtil;

/**
 * Class ModuleAgape
 * @package agape
 */
abstract class ModuleLed extends \Contao\Module
{

    /**
     * get current settings
     * @param $id
     * @return array|false|void
     */
//    public function getSettings($id)
//    {
//        if(!$id) return;
//
//        $dbObj = $this->Database->prepare('SELECT * FROM `tl_agape_properties` WHERE `id`=?')
//            ->limit(1)
//            ->execute($id);
//        return $dbObj->fetchAssoc();
//
//    }


    /**
     * @param string $uuid
     * @return array
     */
    public function getFileInfosAsArray($uuid=''){

        /** @var PageModel $objPage */
        global $objPage;

        $arrFile = array();
        $allowedDownload = StringUtil::trimsplit(',', strtolower(\Config::get('allowedDownload')));
        $objSrc = FilesModel::findByUuid($uuid);
        $objFile = null;

        if ($objSrc !== null)
        {
            $objFile = new File($objSrc->path);

            $strHref = Environment::get('request');

            // Remove an existing file parameter (see #5683)
            if (preg_match('/(&(amp;)?|\?)file=/', $strHref))
            {
                $strHref = preg_replace('/(&(amp;)?|\?)file=[^&]+/', '', $strHref);
            }

            if (!\in_array($objFile->extension, $allowedDownload) || preg_match('/^meta(_[a-z]{2})?\.txt$/', $objFile->basename))
            {
                return $arrFile;
            }

            $arrMeta = $this->getMetaData($objFile->meta, $objFile->language);

            if (empty($arrMeta))
            {
                if ($this->metaIgnore)
                {
                    return $arrFile;
                }
                elseif ($objPage->rootFallbackLanguage !== null)
                {
                    $arrMeta = $this->getMetaData($objFile->meta, $objPage->rootFallbackLanguage);
                }
            }

            // Use the file name as title if none is given
            if ($arrMeta['title'] == '')
            {
                $arrMeta['title'] = StringUtil::specialchars($objFile->basename);
            }

            $strHref .= (strpos($strHref, '?') !== false ? '&amp;' : '?') . 'file=' . \System::urlEncode($objFile->value);

            $arrFile = array
            (
                'link'      => $arrMeta['title'],
                'caption'   => $arrMeta['caption'],
                'title' => StringUtil::specialchars($this->titleText ?: sprintf($GLOBALS['TL_LANG']['MSC']['download'], $objFile->basename)),
                'href' => $strHref,
                'filesize' => $this->getReadableSize($objFile->filesize, 1),
                'icon' => Image::getPath($objFile->icon),
                'mime' => $objFile->mime,
                'extension' => $objFile->extension,
                'path' => $objFile->dirname
            );
        }

        return $arrFile;
    }

    /**
     * @param string $multiSrc
     * @return array
     */
    public function getMultiFileInfosAsArray($multiSrc=''){

        /** @var PageModel $objPage */
        global $objPage;

        $arrFile = array();
        $allowedDownload = \StringUtil::trimsplit(',', strtolower(\Config::get('allowedDownload')));

        $multiSrc = \StringUtil::deserialize($multiSrc);

        $objSrc = \FilesModel::findMultipleByUuids($multiSrc);

        if ($objSrc !== null)
        {
            // Get all files
            while ($objSrc->next()) {

                // Continue if the files has been processed or does not exist
                if (isset($files[$objFiles->path]) || !file_exists(TL_ROOT . '/' . $objFiles->path)) {
                    continue;
                }

                $objFile = new \Contao\File($objSrc->path);

                if (!\in_array($objFile->extension, $allowedDownload) || preg_match('/^meta(_[a-z]{2})?\.txt$/', $objFile->basename))
                {
                    continue;
                }

                $arrMeta = $this->getMetaData($objFiles->meta, $objPage->language);

                if (empty($arrMeta))
                {
                    if ($this->metaIgnore)
                    {
                        continue;
                    }
                    elseif ($objPage->rootFallbackLanguage !== null)
                    {
                        $arrMeta = $this->getMetaData($objFiles->meta, $objPage->rootFallbackLanguage);
                    }
                }

                // Use the file name as title if none is given
                if ($arrMeta['title'] == '')
                {
                    $arrMeta['title'] = \StringUtil::specialchars($objFile->basename);
                }

                $strHref = \Environment::get('request');

                // Remove an existing file parameter (see #5683)
                if (preg_match('/(&(amp;)?|\?)file=/', $strHref)) {
                    $strHref = preg_replace('/(&(amp;)?|\?)file=[^&]+/', '', $strHref);
                }

                $strHref .= (strpos($strHref, '?') !== false ? '&amp;' : '?') . 'file=' . \System::urlEncode($objFile->value);

                $arrFile[] = array
                (
                    'link'      => $arrMeta['title'],
                    'caption'   => $arrMeta['caption'],
                    'title' => \StringUtil::specialchars($this->titleText ?: sprintf($GLOBALS['TL_LANG']['MSC']['download'], $objFile->basename)),
                    'href' => $strHref,
                    'filesize' => $this->getReadableSize($objFile->filesize, 1),
                    'icon' => \Image::getPath($objFile->icon),
                    'mime' => $objFile->mime,
                    'meta'      => $arrMeta,
                    'extension' => $objFile->extension,
                    'path' => $objFile->dirname
                );
            }
        }

        return $arrFile;
    }
}
