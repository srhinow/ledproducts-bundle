<?php

/**
 * PHP version 7
 * @copyright  Sven Rhinow Webentwicklung 2018 <http://www.sr-tag.de>
 * @author     Sven Rhinow
 * @package    ledproducts-bundle
 * @license	   LGPL +3.0
 * @filesource
 */
namespace Srhinow\LedproductsBundle;

use Contao\BackendTemplate;
use Contao\FrontendTemplate;
use Contao\Input;
use Contao\Pagination;

/**
 * Class ModuleLedSerieArticlesList
 */
class ModuleLedSerieArticlesList extends ModuleLed
{
    /**
     * Template
     * @var string
     */
    protected $strTemplate = 'led_serie_articles_list';

    /**
     * Templae for default serie-category
     * @var string
     */
    protected $strCategoryTemplate = 'led_serie_category_list';


    /**
     * Target pages
     * @var array
     */
    protected $arrTargets = array();


    /**
     * Display a wildcard in the back end
     * @return string
     */
    public function generate()
    {
        if (TL_MODE == 'BE')
        {
            $objTemplate = new BackendTemplate('be_wildcard');

            $objTemplate->wildcard = '### LED SERIE ARTICLE LIST ###';

            $objTemplate->title = $this->headline;
            $objTemplate->id = $this->id;
            $objTemplate->link = $this->name;
            $objTemplate->href = 'contao/main.php?do=modules&amp;act=edit&amp;id=' . $this->id;

            return $objTemplate->parse();
        }

        $file = Input::get('file', true);
        // Send the file to the browser and do not send a 404 header (see #4632)
        if ($file != '')
        {
            \Controller::sendFileToBrowser($file);
        }

        // Set the item from the auto_item parameter
        if (!isset($_GET['serie']) && $GLOBALS['TL_CONFIG']['useAutoItem'] && isset($_GET['auto_item']))
        {
            Input::setGet('serie', Input::get('auto_item'));
        }

        // Do not index or cache the page if no category item has been specified
        if (!Input::get('serie'))
        {
            global $objPage;
            $objPage->noSearch = 1;
            $objPage->cache = 0;
            return '';
        }

        // Fallback template
        if (strlen($this->fe_led_template)) $this->strTemplate = $this->fe_led_template;

        //falls spezielles Template aus der aktuellen Serien-Einstellungen gesetzt ist, wird es hier zugewiesen
        $objSerie = LedSeriesModel::findByIdOrAlias(Input::get('serie'));

        // Listen-Umgebendes Templae
        if (strlen($objSerie->articleListTpl)> 0 ) $this->strTemplate = $objSerie->articleListTpl;

        // Ansicht der Tabelle der jeweiligen Kategore
        if (strlen($objSerie->categoryArticleListTpl)> 0 ) $this->strCategoryTemplate = $objSerie->categoryArticleListTpl;

        return parent::generate();
    }


    /**
     * Generate module
     */
    protected function compile()
    {
        /** @var \PageModel $objPage */
        global $objPage;


        $objSerie = LedSeriesModel::findByIdOrAlias(Input::get('serie'));
        if (null === $objSerie)
        {
            /** @var \PageError404 $objHandler */
            $objHandler = new $GLOBALS['TL_PTY']['error_404']();
            $objHandler->generate($objPage->id);
        }

        // Get the items
        $categoryOptions['order'] = 'alias';
        $categoryObj = LedSerieCategoriesModel::findPublishedByPid($objSerie->id,0,$categoryOptions);

        if(null !== $categoryObj) while($categoryObj->next())
        {
            //zur Kategorie gehoerende Artikel holen
            $arrOptions['order'] = strlen($categoryObj->article_sorting)?$categoryObj->article_sorting:'alias';
            $itemsObj = LedSerieArticlesModel::findPublishedByCategory($categoryObj->id, $arrOptions);

            //wenn der Kategorie keine aktiven Artikel angehoeren diese Kategorie ueberspringen
            if(null === $itemsObj) continue;

            //alle Array-Sammlungen zuruecksetzen
            $itemArray = $itemsArray = $filterArr = [];
            $count = 0;

            //Filter-Array-Item setzen wenn nicht schon vorhanden
            $filterArr =
                [
                    'name' => $categoryObj->name,
                    'id' => $categoryObj->id,
                    'alias' => $categoryObj->alias
                ];

            //template überschreiben wenn ein bestimmtes gesetzt wurde
            if(strlen($categoryObj->serieCategoryListTpl) > 0){
                $this->strCategoryTemplate = $categoryObj->serieCategoryListTpl;
            }

            $limit = $itemsObj->count();
            while($itemsObj->next()){
                //row - Class
                $class = 'row_' . $count . (($count == 0) ? ' row_first' : '') . (($count >= ($limit - 1)) ? ' row_last' : '') . ((($count % 2) == 0) ? ' even' : ' odd');

                //Product-Details-Url
                $articleUrl = false;
                if($this->jumpTo)
                {
                    $objProductsPage = \PageModel::findByPk($this->jumpTo);
                    $articleUrl = ampersand( $objProductsPage->getFrontendUrl('/'.$itemsObj->alias) );
                }

                $itemArray = $itemsObj->row();
                $itemArray['class'] = $class;
                $itemArray['detailsUrl'] = $articleUrl;

                //datasheet
                $itemArray['datasheet'] = $this->getFileInfosAsArray($itemsObj->datasheet);

                $itemsArray[] = $itemArray;
            }

            $arrTables[] = $this->parseCategoryTable($objSerie->row(), $itemsArray, $filterArr, $this->strCategoryTemplate);
        }

        $this->Template->headline = $this->headline;
        $this->Template->tables = $arrTables;
        $this->Template->messages = ''; // Backwards compatibility
    }

    /**
     * die Tabelle der jeweiligen Kategorie rendern.
     * @param array $arrSerie
     * @param array $arrItems
     * @param array $arrFilter
     * @return string
     */
    protected function parseCategoryTable($arrSerie = [], $arrItems = [], $arrFilter=[], $template=''){
        $table = '';
        if(strlen($template) < 1) return '';

        $Template = new FrontendTemplate($template);
        $Template->serie = $arrSerie;
        $Template->arrItems = $arrItems;
        $Template->filterArr = $arrFilter;
        $table = $Template->parse();

        return $table;
    }
}
