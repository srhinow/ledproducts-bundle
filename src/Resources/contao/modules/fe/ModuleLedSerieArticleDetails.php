<?php

/**
 * PHP version 7
 * @copyright  Sven Rhinow Webentwicklung 2018 <http://www.sr-tag.de>
 * @author     Sven Rhinow
 * @package    ledproducts-bundle
 * @license	   LGPL +3.0
 * @filesource
 */

namespace Srhinow\LedproductsBundle;

use Contao\Environment;
use Contao\FilesModel;
use Contao\PageModel;
use Contao\System;

/**
 * Class ModuleAgapeProductDetails
 */
class ModuleLedSerieArticleDetails extends ModuleLed
{
    /**
     * Template
     * @var string
     */
    protected $strTemplate = 'led_serie_article_details';

    /**
     * Target pages
     * @var array
     */
    protected $arrTargets = array();


    /**
     * Display a wildcard in the back end
     * @return string
     */
    public function generate()
    {
        if (TL_MODE == 'BE')
        {
            $objTemplate = new \BackendTemplate('be_wildcard');

            $objTemplate->wildcard = '### LED ARTICLE DETAILS ###';

            $objTemplate->title = $this->headline;
            $objTemplate->id = $this->id;
            $objTemplate->link = $this->name;
            $objTemplate->href = 'contao/main.php?do=modules&amp;act=edit&amp;id=' . $this->id;

            return $objTemplate->parse();
        }

        $file = \Input::get('file', true);

        // Send the file to the browser and do not send a 404 header (see #4632)
        if ($file != '')
        {
            \Controller::sendFileToBrowser($file);
        }

        // Set the item from the auto_item parameter
        if (!isset($_GET['article']) && $GLOBALS['TL_CONFIG']['useAutoItem'] && isset($_GET['auto_item']))
        {
            \Input::setGet('article', \Input::get('auto_item'));
        }

        // Do not index or cache the page if no category item has been specified
        if (!\Input::get('article'))
        {
            global $objPage;
            $objPage->noSearch = 1;
            $objPage->cache = 0;
            return '';
        }

        $objArticle = LedSerieArticlesModel::findArticleByIdOrAlias(\Input::get('article'));
        $objSerie = LedSeriesModel::findByIdOrAlias($objArticle->pid);

        //falls spezielles Template aus der aktuellen Serien-Einstellungen gesetzt ist, wird es hier zugewiesen
        if (strlen($objSerie->articleDetailsTpl)> 0 ) $this->strTemplate = $objSerie->articleDetailsTpl;

        // Template aus den Modul-Einstellungen
        if (strlen($this->fe_led_template)) $this->strTemplate = $this->fe_led_template;

        return parent::generate();
    }


    /**
     * Generate module
     */
    protected function compile()
    {
        /** @var \PageModel $objPage */
        global $objPage;

        $objArticle = LedSerieArticlesModel::findArticleByIdOrAlias(\Input::get('article'));

        if ($objArticle === null)
        {
            // Do not index or cache the page
            $objPage->noSearch = 1;
            $objPage->cache = 0;

            // Send a 404 header
            header('HTTP/1.1 404 Not Found');
            $this->Template->messages = '<p class="error">' . sprintf($GLOBALS['TL_LANG']['MSC']['invalidPage'], \Input::get('items')) . '</p>';
            return;
        }

        $objSerie = LedSeriesModel::findByIdOrAlias($objArticle->pid);

        if (null === $objSerie)
        {
            /** @var \PageError404 $objHandler */
            $objHandler = new $GLOBALS['TL_PTY']['error_404']();
            $objHandler->generate($objPage->id);
        }

        $objLedCategory = LedCategoriesModel::findCategoryByIdOrAlias($objSerie->pid);

        if (null === $objLedCategory)
        {
            /** @var \PageError404 $objHandler */
            $objHandler = new $GLOBALS['TL_PTY']['error_404']();
            $objHandler->generate($objPage->id);
        }

        //falls spezielles Template aus der aktuellen Serien-Einstellungen gesetzt ist, wird es hier zugewiesen
        if (strlen($objSerie->articleListTpl)) $this->strTemplate = $this->articleListTpl;

        //passende Kategorie holen
        $filterObj = null;

        if($objArticle->category > 0)
        {
            $filterObj = LedSerieCategoriesModel::findByIdOrAlias($objArticle->category);

            if(is_object($filterObj)) {

                //Class ergaenzen
//                        $class .= ' '.$filterObj->alias;

                //Filter-Array-Item setzen wenn nicht schon vorhanden
                $filterArr = array(
                    'id' => $filterObj->id,
                    'name' => $filterObj->name,
                    'alias' => $filterObj->alias
                );
            }
        }

        // Add the article image as enclosure
        $image = '';
        $objImage = FilesModel::findByUuid($objArticle->image);
        if ($objImage !== null)
        {
            $image = $objImage->path;
        }

        // Add the spectrum image as enclosure
        $spectrumImage = '';
        $objSpectrImage = FilesModel::findByUuid($objArticle->spectrumImage);
        if ($objSpectrImage !== null)
        {
            $spectrumImage = $objSpectrImage->path;
        }

        //datasheet
        $datasheetArr = $this->getFileInfosAsArray($objArticle->datasheet);

        //rayfile
        $rayfileArr = $this->getFileInfosAsArray($objArticle->rayfile);

        //reach_rohs
        $reachrohsArr = $this->getMultiFileInfosAsArray($objLedCategory->reach_rohs);

        $itemArray = $objArticle->row();
        $itemArray['articleImage'] = $image;
        $itemArray['spectrumImage'] = $spectrumImage;
        $itemArray['datasheet'] = $datasheetArr;
        $itemArray['rayfile'] = $rayfileArr;
        $itemArray['reach_rohs'] = $reachrohsArr;

        System::loadLanguageFile('tl_led_serie_articles');
        $itemArray['wavelength_type'] = $GLOBALS['TL_LANG']['tl_led_serie_articles']['wavelength_type_options'][$itemArray['wavelength_type']];

        //Webstores vorbereiten
        $itemArray['webstore_germany'] = unserialize($itemArray['webstore_germany']);
        $itemArray['webstore_global'] = unserialize($itemArray['webstore_global']);

        $this->Template->headline = $this->headline;
        $this->Template->hl = $this->hl;
        $this->Template->item = $itemArray;
        $this->Template->SerieName = $objSerie->name;


        $objPage->pageTitle = $filterArr['name'].' item '.$objArticle->name;

        //SEO-Werte setzen
        if(strlen($objArticle->seo_title) > 0) $objPage->pageTitle = strip_tags(strip_insert_tags($objArticle->seo_title));
        if(strlen($objArticle->seo_keywords) > 0) $GLOBALS['TL_KEYWORDS'] .= strip_tags(strip_insert_tags($objArticle->seo_keywords));
        if(strlen($objArticle->seo_description) > 0) $objPage->description = strip_tags(strip_insert_tags($objArticle->seo_description));
    }


}
