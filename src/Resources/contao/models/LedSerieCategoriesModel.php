<?php

/**
 * for Contao Open Source CMS
 *
 * Copyright (c) 2018 Sven Rhinow
 *
 * @package ledproducts-bundle
 * @license http://www.gnu.org/licenses/lgpl-3.0.html LGPL
 */

namespace Srhinow\LedproductsBundle;

use Contao\Model;

class LedSerieCategoriesModel extends Model
{
    /**
     * Table name
     * @var string
     */
    protected static $strTable = 'tl_led_serie_categories';

    /**
     * Find published news items by their parent ID and ID or alias
     *
     * @param mixed $varId      The numeric ID or alias name
     * @param array $arrOptions An optional options array
     *
     * @return LedSerieCategoriesModel|null The findCategoryByIdOrAlias or null if there are no category
     */
    public static function findCategoryByIdOrAlias($varId, array $arrOptions=array())
    {
        $t = static::$strTable;
        $arrColumns = !is_numeric($varId) ? array("$t.alias=?") : array("$t.id=?");

        return static::findOneBy($arrColumns, $varId, $arrOptions);
    }

    /**
     * Find categories for pagination-list
     * @param int $intLimit
     * @param int $intOffset
     * @param array $filter
     * @param array $arrOptions
     * @return \Model\Collection|null|static
     */
    public static function findCategories($intLimit=0, $intOffset=0, array $filter=array(), array $arrOptions=array())
    {
        $t = static::$strTable;
        $arrColumns = (count($filter) > 0)? $filter : null;

        if (!isset($arrOptions['order']))
        {
            $arrOptions['order'] = "$t.id DESC";
        }

        $arrOptions['limit']  = $intLimit;
        $arrOptions['offset'] = $intOffset;


        return static::findBy($arrColumns, null, $arrOptions);
    }

    /**
     * Find published products items by their parent ID
     *
     * @param integer $intId      The agape product category ID
     * @param integer $intLimit   An optional limit
     * @param array   $arrOptions An optional options array
     *
     * @return \Model\Collection|LedSerieCategoriesModel[]|LedSerieCategoriesModel|null A collection of models or null if there are no news
     */
    public static function findPublishedByPid($intId, $intLimit=0, array $arrOptions=array())
    {
        $t = static::$strTable;
        $arrColumns = array("$t.pid=?");

        if (!isset($arrOptions['order']))
        {
            $arrOptions['order'] = "$t.sorting DESC";
        }

        if (!BE_USER_LOGGED_IN)
        {
            $arrColumns[] = "$t.published='1'";
        }

        if ($intLimit > 0)
        {
            $arrOptions['limit'] = $intLimit;
        }

        return static::findBy($arrColumns, $intId, $arrOptions);
    }

    /**
     * Count all category items
     *
     * @param array $filter
     * @param array $arrOptions
     * @return integer The number of news items
     */
    public static function countEntries(array $filter=array(), array $arrOptions=array())
    {
//        $t = static::$strTable;
        $arrColumns = (count($filter) > 0)? $filter : null;

        return static::countBy($arrColumns, null, $arrOptions);
    }
}
