<?php

/**
 * for Contao Open Source CMS
 *
 * Copyright (c) 2018 Sven Rhinow
 *
 * @package ledproducts-bundle
 * @license http://www.gnu.org/licenses/lgpl-3.0.html LGPL
 */
namespace Srhinow\LedproductsBundle;

use Contao\Model;

class LedSeriesModel extends Model
{
    /**
     * Table name
     * @var string
     */
    protected static $strTable = 'tl_led_series';

    /**
     * Find published series items by their ID or alias
     *
     * @param mixed $varId      The numeric ID or alias name
     * @param array $arrOptions An optional options array
     *
     * @return LedSeriesModel|null The LedSeriesModel or null if there are no series
     */
    public static function findSerieByIdOrAlias($varId, array $arrOptions=array())
    {
        $t = static::$strTable;
        $arrColumns = !is_numeric($varId) ? array("$t.alias=?") : array("$t.id=?");

        return static::findOneBy($arrColumns, $varId, $arrOptions);
    }

    /**
     * Find published series items by their parent ID
     *
     * @param integer $intId      The led category ID
     * @param integer $intLimit   An optional limit
     * @param integer $intOffset
     * @param array   $filter
     * @param array   $arrOptions An optional options array
     *
     * @return \Model\Collection|null|static
     */
    public static function findPublishedByPid($intId, $intLimit=0, $intOffset=0, array $filter=array(), array $arrOptions=array())
    {
        $t = static::$strTable;

        $arrColumns = (count($filter) > 0)? $filter : null;
        $arrColumns[] = "$t.pid=?";

        if (!isset($arrOptions['order']))
        {
            $arrOptions['order'] = "$t.sorting DESC";
        }

        if (!BE_USER_LOGGED_IN)
        {
            $arrColumns[] = "$t.published='1'";
        }

        $arrOptions['limit']  = $intLimit;
        $arrOptions['offset'] = $intOffset;

        return static::findBy($arrColumns, $intId, $arrOptions);
    }

    /**
     * Count all published series be there category id
     *
     * @param integer $intId      The led category ID
     * @param array $filter
     * @param array $arrOptions
     * @return integer
     */
    public static function countPublishedByPid($intId, array $filter=array(), array $arrOptions=array())
    {
        $t = static::$strTable;
        $arrColumns = (count($filter) > 0)? $filter : null;
        $arrColumns[] = "$t.pid=".$intId;

        if (!BE_USER_LOGGED_IN)
        {
            $arrColumns[] = "$t.published='1'";
        }

        return static::countBy($arrColumns, null, $arrOptions);
    }
}
