<?php

/**
 * for Contao Open Source CMS
 *
 * Copyright (c) 2018 Sven Rhinow
 *
 * @package ledproducts-bundle
 * @license http://www.gnu.org/licenses/lgpl-3.0.html LGPL
 */

namespace Srhinow\LedproductsBundle;

use Contao\Model;

class LedCategoriesModel extends Model
{
    /**
     * Table name
     * @var string
     */
    protected static $strTable = 'tl_led_categories';

    /**
     * Find published led category items by their ID or alias
     *
     * @param mixed $varId      The numeric ID or alias name
     * @param array $arrOptions An optional options array
     *
     * @return LedCategoriesModel|null The findCategoryByIdOrAlias or null if there are no category
     */
    public static function findCategoryByIdOrAlias($varId, array $arrOptions=array())
    {
        $t = static::$strTable;
        $arrColumns = !is_numeric($varId) ? array("$t.alias=?") : array("$t.id=?");

        return static::findOneBy($arrColumns, $varId, $arrOptions);
    }

    /**
     * Find categories for pagination-list
     * @param int $intLimit
     * @param int $intOffset
     * @param array $filter
     * @param array $arrOptions
     * @return \Model\Collection|null|static
     */
    public static function findCategories($intLimit=0, $intOffset=0, array $filter=array(), array $arrOptions=array())
    {
        $t = static::$strTable;
        $arrColumns = (count($filter) > 0)? $filter : null;

        if (!isset($arrOptions['order']))
        {
            $arrOptions['order'] = "$t.id DESC";
        }

        $arrOptions['limit']  = $intLimit;
        $arrOptions['offset'] = $intOffset;


        return static::findBy($arrColumns, null, $arrOptions);
    }

    /**
     * Count all category items
     *
     * @param array $filter
     * @param array $arrOptions
     * @return integer The number of led category items
     */
    public static function countEntries(array $filter=array(), array $arrOptions=array())
    {
//        $t = static::$strTable;
        $arrColumns = (count($filter) > 0)? $filter : null;

        return static::countBy($arrColumns, null, $arrOptions);
    }
}
