<?php

/**
 * PHP version > 7.1
 * @copyright  Sven Rhinow Webentwicklung 2017 <http://www.sr-tag.de>
 * @author     Sven Rhinow
 * @package    ledproducts-bundle
 * @license    LGPL
 * @filesource
 */


/**
 * Table tl_led_series
 */
$GLOBALS['TL_DCA']['tl_led_series'] = array
(

    // Config
    'config' => array
    (
        'dataContainer'               => 'Table',
        'ctable'                      => array('tl_led_serie_categories','tl_led_serie_articles'),
        'enableVersioning'            => true,
        'sql' => array
        (
            'keys' => array
            (
                'id' => 'primary',
                'pid' => 'index',
                'sorting' => 'index',
            )
        )
    ),
    // List
    'list' => array
    (
        'sorting' => array
        (
            'mode'                    => 2,
            'fields'                  => array('pid'),
            'flag'					  => 1,
            'panelLayout'             => 'filter;sort,search,limit'
        ),
        'label' => array
        (
            'fields'                  => array('name'),
            'format'                  => '%s',
            'label_callback'		  => array('tl_led_series', 'listEntries')
        ),
        'global_operations' => array
        (
            'categories' => array
            (
                'label'               => &$GLOBALS['TL_LANG']['tl_led_series']['product_categories'],
                'href'                => 'table=tl_led_categories',
                'class'               => 'header_icon',
                'icon'  			  => '/bundles/srhinowledproducts/icons/categories.png',
                'attributes'          => 'onclick="Backend.getScrollOffset();"'
            ),
            'filter' => array
            (
                'label'               => &$GLOBALS['TL_LANG']['tl_led_series']['product_filter'],
                'href'                => 'table=tl_led_filter',
                'class'               => 'header_icon',
                'icon'  			  => '/bundles/srhinowledproducts/icons/filter.png',
                'attributes'          => 'onclick="Backend.getScrollOffset();"'
            ),
            'all' => array
            (
                'label'               => &$GLOBALS['TL_LANG']['MSC']['all'],
                'href'                => 'act=select',
                'class'               => 'header_edit_all',
                'attributes'          => 'onclick="Backend.getScrollOffset();"',
            )
        ),
        'operations' => array
        (
            'edit' => array
            (
                'label'               => &$GLOBALS['TL_LANG']['tl_led_series']['edit_article'],
                'href'                => 'table=tl_led_serie_articles',
                'icon'                => 'edit.svg'
            ),
            'editheader' => array
            (
                'label'               => &$GLOBALS['TL_LANG']['tl_led_series']['edit'],
                'href'                => 'act=edit',
                'icon'                => 'header.svg'
            ),
            'copy' => array
            (
                'label'               => &$GLOBALS['TL_LANG']['tl_led_series']['copy'],
                'href'                => 'act=copy',
                'icon'                => 'copy.svg',
            ),
            'delete' => array
            (
                'label'               => &$GLOBALS['TL_LANG']['tl_led_series']['delete'],
                'href'                => 'act=delete',
                'icon'                => 'delete.svg',
                'attributes'          => 'onclick="if (!confirm(\'' . $GLOBALS['TL_LANG']['MSC']['deleteConfirm'] . '\')) return false; Backend.getScrollOffset();"',
            ),
            'toggle' => array
            (
                'label'               => &$GLOBALS['TL_LANG']['tl_led_series']['toggle'],
                'icon'                => 'visible.svg',
                'attributes'          => 'onclick="Backend.getScrollOffset();return AjaxRequest.toggleVisibility(this,%s)"',
                'button_callback'     => array('tl_led_series', 'toggleIcon')
            ),
            'categories' => array
            (
                'label'  => &$GLOBALS['TL_LANG']['tl_led_series']['serie_categories'],
                'href'   => 'table=tl_led_serie_categories',
                'icon'   => 'bundles/srhinowledproducts/icons/categories.png',
            ),
        )
    ),
    // Palettes
    'palettes' => array
    (
        '__selector__'                => array('addImage','addTeaserImage','addGallery'),
        'default' => '
            {data_legend},name,alias,pid,filter,teaser,description,features;
            {image_legend:hide},addImage,addTeaserImage;
            {gallery_legend:hide},addGallery;
            {seo_legend:hide},seo_title,seo_keywords,seo_description;
            {template_legend:hide},serieListTpl,serieDetailTpl,articleListTpl,categoryArticleListTpl,articleDetailsTpl;
            {extend_legend:hide},published,set_default_categories,sorting'
    ),
    // Subpalettes
    'subpalettes' => array
    (
        'addImage'                    => 'image,alt,clickForBigger',
        'addTeaserImage'              => 'teaserImage',
        'addGallery'                    => 'multiSRC',
//        'published'                   => 'start,stop'
    ),
    // Fields
    'fields' => array
    (
        'id' => array
        (
            'sql'                     => "int(10) unsigned NOT NULL auto_increment"
        ),
        'pid' => array
        (
            'label'                   => &$GLOBALS['TL_LANG']['tl_led_series']['category'],
            'exclude'                 => true,
            'filter'                  => true,
            'sorting'                  => true,
            'inputType'               => 'select',
            'foreignKey'              => 'tl_led_categories.name',
            'eval'                    => array('mandatory'=>true, 'chosen'=>true, 'tl_class'=>'w50'),
            'sql'                     => "int(10) unsigned NOT NULL default '0'"
        ),
        'filter' => array
        (
            'label'                   => &$GLOBALS['TL_LANG']['tl_led_series']['filter'],
            'exclude'                 => true,
            'sorting'                  => true,
            'inputType'               => 'select',
            'foreignKey'              => 'tl_led_filter.name',
            'eval'                    => array('mandatory'=>true, 'chosen'=>true, 'tl_class'=>'w50'),
            'sql'                     => "int(10) unsigned NOT NULL default '0'"
        ),
        'tstamp' => array
        (
            'sql'                     => "int(10) unsigned NOT NULL default '0'"
        ),
        'modify' => array
        (
            'sql'                     => "int(10) unsigned NOT NULL default '0'"
        ),
        'sorting' => array
        (
            'label'                   => &$GLOBALS['TL_LANG']['tl_led_series']['sorting'],
            'exclude'                 => true,
            'sorting'                 => true,
            'inputType'               => 'text',
            'eval'                    => array('mandatory'=>false, 'maxlength'=>255, 'decodeEntities'=>true, 'tl_class'=>'clr w50'),
            'sql'                     => "int(10) unsigned NOT NULL default '0'",
        ),
        'name' => array
        (
            'label'                   => &$GLOBALS['TL_LANG']['tl_led_series']['name'],
            'exclude'                 => true,
            'search'                  => true,
            'sorting'                 => true,
            'inputType'               => 'text',
            'eval'                    => array('mandatory'=>true, 'maxlength'=>255, 'decodeEntities'=>true, 'tl_class'=>'w50'),
            'sql'                     => "varchar(255) NOT NULL default ''"
        ),
        'alias' => array
        (
            'label'                   => &$GLOBALS['TL_LANG']['tl_led_series']['alias'],
            'exclude'                 => true,
            'search'                  => true,
            'sorting'                 => true,
            'inputType'               => 'text',
            'eval'                    => array('rgxp'=>'alnum', 'doNotCopy'=>true, 'spaceToUnderscore'=>true, 'maxlength'=>128, 'tl_class'=>'w50'),
            'save_callback' => array
            (
                array('tl_led_series', 'generateAlias')
            ),
            'sql'                     => "varchar(128) NOT NULL default ''"
        ),
        'teaser' => array
        (
            'label'                   => &$GLOBALS['TL_LANG']['tl_led_series']['teaser'],
            'exclude'                 => true,
            'search'                  => true,
            'inputType'               => 'textarea',
            'eval'                    => array('tl_class'=>'clr'),
            'sql'                     => "text NULL"
        ),
        'description' => array
        (
            'label'                   => &$GLOBALS['TL_LANG']['tl_led_series']['description'],
            'exclude'                 => true,
            'search'                  => true,
            'inputType'               => 'textarea',
            'eval'                    => array('rte'=>'tinyMCE', 'tl_class'=>'clr'),
            'sql'                     => "text NULL"
        ),
        'features' => array(
            'label'                   => &$GLOBALS['TL_LANG']['tl_led_series']['features'],
            'exclude'                 => true,
            'inputType'               => 'textarea',
            'default'                 => '',
            'search'                  => true,
            'eval'                    => array('rte'=>'tinyMCE', 'tl_class'=>'clr'),
            'sql'					  => "text NULL"
        ),
        'addImage' => array
        (
            'label'                   => &$GLOBALS['TL_LANG']['tl_led_series']['addImage'],
            'exclude'                 => true,
            'inputType'               => 'checkbox',
            'eval'                    => array('submitOnChange'=>true),
            'sql'                     => "char(1) NOT NULL default ''"
        ),
        'image' => array
        (
            'label'                   => &$GLOBALS['TL_LANG']['tl_led_series']['image'],
            'exclude'                 => true,
            'inputType'               => 'fileTree',
            'eval'                    => array('filesOnly'=>true, 'fieldType'=>'radio', 'mandatory'=>true, 'tl_class'=>'clr'),
            'sql'                     => "binary(16) NULL"
        ),
        'alt' => array
        (
            'label'                   => &$GLOBALS['TL_LANG']['tl_led_series']['alt'],
            'exclude'                 => true,
            'search'                  => true,
            'inputType'               => 'text',
            'eval'                    => array('maxlength'=>255, 'tl_class'=>'long'),
            'sql'                     => "varchar(255) NOT NULL default ''"
        ),
        'clickForBigger' => array
        (
            'label'                   => &$GLOBALS['TL_LANG']['tl_led_series']['clickForBigger'],
            'exclude'                 => true,
            'inputType'               => 'checkbox',
            'eval'                    => array('tl_class'=>'clr'),
            'sql'                     => "char(1) NOT NULL default ''"
        ),
        'addTeaserImage' => array
        (
            'label'                   => &$GLOBALS['TL_LANG']['tl_led_series']['addTeaserImage'],
            'exclude'                 => true,
            'inputType'               => 'checkbox',
            'eval'                    => array('submitOnChange'=>true),
            'sql'                     => "char(1) NOT NULL default ''"
        ),
        'teaserImage' => array
        (
            'label'                   => &$GLOBALS['TL_LANG']['tl_led_series']['teaserImage'],
            'exclude'                 => true,
            'inputType'               => 'fileTree',
            'eval'                    => array('filesOnly'=>true, 'fieldType'=>'radio', 'mandatory'=>true, 'tl_class'=>'clr'),
            'sql'                     => "binary(16) NULL"
        ),
        'addGallery' => array
        (
            'label'                   => &$GLOBALS['TL_LANG']['tl_led_series']['addGallery'],
            'exclude'                 => true,
            'inputType'               => 'checkbox',
            'eval'                    => array('submitOnChange'=>true),
            'sql'                     => "char(1) NOT NULL default ''"
        ),
        'multiSRC' => array
        (
            'label'                   => &$GLOBALS['TL_LANG']['tl_led_series']['multiSRC'],
            'exclude'                 => true,
            'inputType'               => 'fileTree',
            'eval'                    => array('multiple'=>true, 'fieldType'=>'checkbox', 'orderField'=>'orderSRC', 'files'=>true,'isGallery'=>true,'extensions'=>Config::get('validImageTypes')),
            'sql'                     => "blob NULL"
        ),
        'orderSRC' => array
        (
            'label'                   => &$GLOBALS['TL_LANG']['tl_led_series']['orderSRC'],
            'sql'                     => "blob NULL"
        ),
        'serieListTpl' => array
        (
            'label'                   => &$GLOBALS['TL_LANG']['tl_led_series']['serieListTpl'],
            'exclude'                 => true,
            'inputType'               => 'select',
            'options_callback'        => array('tl_led_series', 'getSerieArtikelTemplates'),
            'eval'                    => array('includeBlankOption'=>true, 'chosen'=>true, 'tl_class'=>'w50'),
            'sql'                     => "varchar(64) NOT NULL default ''"
        ),
        'serieDetailTpl' => array
        (
            'label'                   => &$GLOBALS['TL_LANG']['tl_led_series']['serieDetailTpl'],
            'exclude'                 => true,
            'inputType'               => 'select',
            'options_callback'        => array('tl_led_series', 'getSerieArtikelTemplates'),
            'eval'                    => array('includeBlankOption'=>true, 'chosen'=>true, 'tl_class'=>'w50'),
            'sql'                     => "varchar(64) NOT NULL default ''"
        ),
        'articleListTpl' => array
        (
            'label'                   => &$GLOBALS['TL_LANG']['tl_led_series']['articleListTpl'],
            'exclude'                 => true,
            'inputType'               => 'select',
            'options_callback'        => array('tl_led_series', 'getSerieArtikelTemplates'),
            'eval'                    => array('includeBlankOption'=>true, 'chosen'=>true, 'tl_class'=>'w50'),
            'sql'                     => "varchar(64) NOT NULL default ''"
        ),
        'categoryArticleListTpl' => array
        (
            'label'                   => &$GLOBALS['TL_LANG']['tl_led_series']['categoryArticleListTpl'],
            'exclude'                 => true,
            'inputType'               => 'select',
            'options_callback'        => array('tl_led_serie_categories', 'getSerieCategoryTemplates'),
            'eval'                    => array('includeBlankOption'=>true, 'chosen'=>true, 'tl_class'=>'w50'),
            'sql'                     => "varchar(64) NOT NULL default ''"
        ),
        'articleDetailsTpl' => array
        (
            'label'                   => &$GLOBALS['TL_LANG']['tl_led_series']['articleDetailsTpl'],
            'exclude'                 => true,
            'inputType'               => 'select',
            'options_callback'        => array('tl_led_series', 'getSerieArtikelTemplates'),
            'eval'                    => array('includeBlankOption'=>true, 'chosen'=>true, 'tl_class'=>'w50'),
            'sql'                     => "varchar(64) NOT NULL default ''"
        ),
        'published' => array
        (
            'label'                   => &$GLOBALS['TL_LANG']['tl_led_series']['published'],
            'filter'                  => true,
            'sorting'                 => true,
            'inputType'               => 'checkbox',
            'flag'                    => 11,
            'eval'                    => array('tl_class'=>'w50'),
            'sql'                     => "char(1) NOT NULL default ''"
        ),
        'set_default_categories' => array
        (
            'label'                   => &$GLOBALS['TL_LANG']['tl_led_series']['set_default_categories'],
            'exclude'                 => true,
            'inputType'               => 'checkbox',
            'sql'					  => "char(1) NOT NULL default ''",
            'ignoreDiff'			=> true,
            'save_callback' => array
            (
                array('tl_led_series', 'setDefaultSeriesCategories')
            )
        ),
        'seo_title' => array(
            'label'                   => &$GLOBALS['TL_LANG']['tl_led_series']['seo_title'],
            'exclude'                 => true,
            'inputType'               => 'text',
            'search'                  => true,
            'eval'                    => array('decodeEntities'=>true, 'maxlength'=>255,'tl_class'=>'long clr'),
            'sql'                     => "varchar(255) NOT NULL default ''"
        ),
        'seo_keywords' => array(
            'label'                   => &$GLOBALS['TL_LANG']['tl_led_series']['seo_keywords'],
            'exclude'                 => true,
            'inputType'               => 'textarea',
            'search'                  => true,
            'eval'                    => array('style'=>'height:60px', 'decodeEntities'=>true),
            'sql'                     => "text NULL"
        ),
        'seo_description' => array(
            'label'                   => &$GLOBALS['TL_LANG']['tl_led_series']['seo_description'],
            'exclude'                 => true,
            'inputType'               => 'textarea',
            'search'                  => true,
            'eval'                    => array('style'=>'height:60px', 'decodeEntities'=>true, 'maxlength'=>180,'tl_class'=>'clr'),
            'sql'                     => "varchar(180) NOT NULL default ''"
        )
    )
);


/**
 * Class tl_led_series
 */
class tl_led_series extends \Backend
{
    /**
     * Import the back end user object
     */
    public function __construct()
    {
        parent::__construct();
        $this->import('BackendUser', 'User');
    }

    /**
     * set default serie-categories from parent categorie-settings if Checkbox selected
     * @var object
     */
    public function setDefaultSeriesCategories($varValue, \DataContainer $dc)
    {
        if($varValue !== 1) return '';

        $this->import('Database');

        $objLedCat = \Srhinow\LedproductsBundle\LedCategoriesModel::findCategoryByIdOrAlias($dc->activeRecord->pid);

        if(is_object($objLedCat))
        {
            $arrCats = unserialize($objLedCat->seriecatitems);

            $sortingindex = 5000;

            if(is_array($arrCats) && count($arrCats) > 0)

                //alte Kategorien löschen
                $this->Database->prepare('DELETE FROM `tl_led_serie_categories` WHERE `pid`=?')->execute($dc->id);

                //neue Kategorien setzen
                foreach($arrCats as $cat)
                {
                    $sortingindex = $sortingindex - 125;
                    $set = array(
                        'pid' => $dc->id,
                        'tstamp' => time(),
                        'modify' => time(),
                        'sorting' => $sortingindex,
                        'name' => $cat,
                        'alias' => $this->generateSeriesCategoryAlias($cat, $dc),
                        'published' => '1'
                    );

                    $this->Database->prepare('INSERT INTO `tl_led_serie_categories` %s')->set($set)->execute();

                    //  $newPostenID = $objNewSql->insertId;
                    //  print_r($newPostenID); exit();
                }
        }



        $this->Database->prepare('UPDATE `tl_led_series` SET `set_default_categories`="" WHERE `id`=?')->execute($dc->id);

        return '';
    }
    /**
     * Auto-generate an alias if it has not been set yet
     * @param $varValue
     * @param DataContainer $dc
     * @return string
     * @throws Exception
     */
    public function generateSeriesCategoryAlias($varValue, DataContainer $dc)
    {
        $autoAlias = false;

        // Generate an alias if there is none
        if ($varValue == '')
        {
            $autoAlias = true;
            $varValue = standardize(\StringUtil::restoreBasicEntities($dc->activeRecord->name));
        }

        $objAlias = $this->Database->prepare("SELECT id FROM tl_led_serie_categories WHERE id=? OR alias=?")
            ->execute($dc->id, $varValue);

        // Check whether the page alias exists
        if ($objAlias->numRows > 1)
        {
            $varValue .= '-' . $dc->id;
        }

        return $varValue;
    }

    /**
     * Auto-generate an alias if it has not been set yet
     * @param $varValue
     * @param DataContainer $dc
     * @return string
     * @throws Exception
     */
    public function generateAlias($varValue, DataContainer $dc)
    {
        $autoAlias = false;

        // Generate an alias if there is none
        if ($varValue == '')
        {
            $autoAlias = true;
            $varValue = standardize(\StringUtil::restoreBasicEntities($dc->activeRecord->name));
        }

        $objAlias = $this->Database->prepare("SELECT id FROM tl_led_series WHERE id=? OR alias=?")
            ->execute($dc->id, $varValue);

        // Check whether the page alias exists
        if ($objAlias->numRows > 1)
        {
            if (!$autoAlias)
            {
                throw new Exception(sprintf($GLOBALS['TL_LANG']['ERR']['aliasExists'], $varValue));
            }

            $varValue .= '-' . $dc->id;
        }

        return $varValue;
    }

    /**
     * Return the link picker wizard
     *
     * @param DataContainer $dc
     *
     * @return string
     */
    public function pagePicker(DataContainer $dc)
    {
        return ' <a href="' . (($dc->value == '' || strpos($dc->value, '{{link_url::') !== false) ? 'contao/page.php' : 'contao/file.php') . '?do=' . Input::get('do') . '&amp;table=' . $dc->table . '&amp;field=' . $dc->field . '&amp;value=' . rawurlencode(str_replace(array('{{link_url::', '}}'), '', $dc->value)) . '&amp;switch=1' . '" title="' . specialchars($GLOBALS['TL_LANG']['MSC']['pagepicker']) . '" onclick="Backend.getScrollOffset();Backend.openModalSelector({\'width\':768,\'title\':\'' . specialchars(str_replace("'", "\\'", $GLOBALS['TL_DCA'][$dc->table]['fields'][$dc->field]['label'][0])) . '\',\'url\':this.href,\'id\':\'' . $dc->field . '\',\'tag\':\'ctrl_'. $dc->field . ((Input::get('act') == 'editAll') ? '_' . $dc->id : '') . '\',\'self\':this});return false">' . Image::getHtml('pickpage.gif', $GLOBALS['TL_LANG']['MSC']['pagepicker'], 'style="vertical-align:top;cursor:pointer"') . '</a>';
    }

    public function convertImagePath()
    {
        $dwlObj = $this->Database->prepare('SELECT * FROM `tl_led_series`')->execute();

        if($dwlObj->numRows > 0)
        {
            while($dwlObj->next())
            {
                if($dwlObj->file !== NULL) continue;
                $strFile = $dwlObj->dl_url;
                $objFile = \FilesModel::findByPath($strFile);

                // Existing file is being replaced (see #4818)
                if ($objFile !== null)
                {

                    $objFile->tstamp = time();
                    $objFile->path   = $strFile;
                    $objFile->hash   = md5_file(TL_ROOT . '/' . $strFile);
                    $objFile->save();

                    $strUuid = $objFile->uuid;
                }
                else
                {
                    $strUuid = \Dbafs::addResource($strFile)->uuid;
                }

                $set = array('file' => $strUuid);
                $this->Database->prepare('UPDATE `tl_led_series` %s WHERE `id`=?')->set($set)->execute($dwlObj->id);
            }
        }
    }

    /**
     * modify list-view
     * @param array
     * @param string
     * @return string
     */
    public function listEntries($row, $label)
    {
        // exit();
        if ($row['image'] != '')
        {
            $objModel = \FilesModel::findByUuid($row['image']);
            $thumbUrl = \Image::get($objModel->path,100,100);

            if ($objModel === null)
            {
                // print_r($label);
                $label = $row['name'];
            }
            else
            {
                $label =  sprintf('<img src="%s" alt="" title="%s" style="float:left;"> <div style="vertical-align:middle; display:inline-block; margin-left:30px; margin-top: 40px; font-size: 14px;">%s</div>',$thumbUrl, $row['name'],$row['name']);
            }
        }

        return sprintf('<div style="float:left">%s</div>',$label) . "\n";
    }

    /**
     * Return the "toggle visibility" button
     *
     * @param array  $row
     * @param string $href
     * @param string $label
     * @param string $title
     * @param string $icon
     * @param string $attributes
     *
     * @return string
     */
    public function toggleIcon($row, $href, $label, $title, $icon, $attributes)
    {
        if (strlen(Input::get('tid')))
        {
            $this->toggleVisibility(Input::get('tid'), (Input::get('state') == 1), (@func_get_arg(12) ?: null));
            $this->redirect($this->getReferer());
        }

        // Check permissions AFTER checking the tid, so hacking attempts are logged
        if (!$this->User->hasAccess('tl_led_series::published', 'alexf'))
        {
            return '';
        }

        $href .= '&amp;tid='.$row['id'].'&amp;state='.($row['published'] ? '' : 1);

        if (!$row['published'])
        {
            $icon = 'invisible.gif';
        }

        return '<a href="'.$this->addToUrl($href).'" title="'.specialchars($title).'"'.$attributes.'>'.Image::getHtml($icon, $label, 'data-state="' . ($row['published'] ? 1 : 0) . '"').'</a> ';
    }


    /**
     * Disable/enable a user group
     *
     * @param integer       $intId
     * @param boolean       $blnVisible
     * @param DataContainer $dc
     */
    public function toggleVisibility($intId, $blnVisible, DataContainer $dc=null)
    {
        // Set the ID and action
        Input::setGet('id', $intId);
        Input::setGet('act', 'toggle');

        if ($dc)
        {
            $dc->id = $intId; // see #8043
        }

        $objVersions = new Versions('tl_led_series', $intId);
        $objVersions->initialize();

        // Trigger the save_callback
        if (is_array($GLOBALS['TL_DCA']['tl_led_series']['fields']['published']['save_callback']))
        {
            foreach ($GLOBALS['TL_DCA']['tl_led_series']['fields']['published']['save_callback'] as $callback)
            {
                if (is_array($callback))
                {
                    $this->import($callback[0]);
                    $blnVisible = $this->{$callback[0]}->{$callback[1]}($blnVisible, ($dc ?: $this));
                }
                elseif (is_callable($callback))
                {
                    $blnVisible = $callback($blnVisible, ($dc ?: $this));
                }
            }
        }

        // Update the database
        $this->Database->prepare("UPDATE tl_led_series SET tstamp=". time() .", published='" . ($blnVisible ? '1' : '') . "' WHERE id=?")
            ->execute($intId);

        $objVersions->create();
    }

    /**
     * Return all module templates as array
     *
     * @return array
     */
    public function getSerieArtikelTemplates()
    {
        return $this->getTemplateGroup('led_serie');
    }
}
