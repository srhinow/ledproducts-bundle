<?php

/**
 * TL_ROOT/vendor/srhinow/ledproducts-bundle/src/Recources/contao/modules/languages/de/modules.php
 *
 * Contao extension: ledproducts-bundle
 * Deutsch translation file
 *
 * Copyright  Sven Rhinow Webentwicklung 2017 <http://www.sr-tag.de>
 * Author     Sven Rhinow
 * @license    LGPL-3.0+
 * Translator: Sven Rhinow (sr-tag)
 */

/**
 * Back end modules
 */
$GLOBALS['TL_LANG']['MOD']['led'] = array('LED', '');
$GLOBALS['TL_LANG']['MOD']['led_products'] = array('LED-Produkte', 'led-Kategorien und dazugehörende Serien verwalten.');
$GLOBALS['TL_LANG']['MOD']['led_properties'] = array('Einstellungen', 'Dieses Modul erlaubt es Ihnen alle Einstellungen von LED-Produkte zu verwalten.');

/**
 * Front end modules
 */
$GLOBALS['TL_LANG']['FMD']['led']      = 'LED-Produkte';
$GLOBALS['TL_LANG']['FMD']['led_series_list']    = array('LED Serienliste', 'Zeigt alle veröffentlichten Serien.');
$GLOBALS['TL_LANG']['FMD']['led_serie_articles_list']    = array('Artikel-Liste einer Serien-Kategorie', 'Zeigt alle veröffentlichten Artikel einer Serien-Kategorie an.');
$GLOBALS['TL_LANG']['FMD']['led_article_details']    = array('Artikel-Detailansicht', 'eine Detailansicht für einen Artikel einer LED-Serienkategorie.');


