<?php
/**
 * TL_ROOT/vendor/srhinow/ledproducts-bundle/src/Recources/contao/modules/languages/de/tl_led_filter.php
 *
 * Contao extension: ledproducts-bundle
 * Deutsch translation file
 *
 * Copyright  Sven Rhinow Webentwicklung 2017 <http://www.sr-tag.de>
 * Author     Sven Rhinow
 * @license    LGPL-3.0+
 * Translator: Sven Rhinow (sr-tag)
 */

/** Legends */
$GLOBALS['TL_LANG']['tl_led_filter']['data_legend'] = "Filter-Daten";
$GLOBALS['TL_LANG']['tl_led_filter']['extend_legend'] = "weitere Einstellungen";

/** Fields */
$GLOBALS['TL_LANG']['tl_led_filter']['name']['0'] = "Name";
$GLOBALS['TL_LANG']['tl_led_filter']['name']['1'] = "Name der Kategorie.";
$GLOBALS['TL_LANG']['tl_led_filter']['alias']['0'] = "Alias";
$GLOBALS['TL_LANG']['tl_led_filter']['alias']['1'] = "Alias, um auf die Kategorie verweisen zu können.";
$GLOBALS['TL_LANG']['tl_led_filter']['sorting']['0'] = "Sortierung (Zahl)";
$GLOBALS['TL_LANG']['tl_led_filter']['published']['0'] = "auf Website anzeigen";
//$GLOBALS['TL_LANG']['tl_led_filter']['published']['1'] = "wird im Quelltext geschrieben und hilft Brailgeräte und Google ;)";

/** Actions */
$GLOBALS['TL_LANG']['tl_led_filter']['new']['0'] = "Neuen Filter";
$GLOBALS['TL_LANG']['tl_led_filter']['new']['1'] = "Neuen Filter erstellen.";
$GLOBALS['TL_LANG']['tl_led_filter']['show']['0'] = "Einzelheiten zum Filter";
$GLOBALS['TL_LANG']['tl_led_filter']['show']['1'] = "Einzelheiten zum Filter ID %s anzeigen";
$GLOBALS['TL_LANG']['tl_led_filter']['edit']['0'] = "Filter bearbeiten";
$GLOBALS['TL_LANG']['tl_led_filter']['edit']['1'] = "Filter ID %s bearbeiten";
$GLOBALS['TL_LANG']['tl_led_filter']['copy']['0'] = "Filter kopieren";
$GLOBALS['TL_LANG']['tl_led_filter']['copy']['1'] = "Filter ID %s kopieren";
$GLOBALS['TL_LANG']['tl_led_filter']['cut']['0'] = "Filter verschieben";
$GLOBALS['TL_LANG']['tl_led_filter']['cut']['1'] = "Filter ID %s verschieben";
$GLOBALS['TL_LANG']['tl_led_filter']['delete']['0'] = "Filter löschen";
$GLOBALS['TL_LANG']['tl_led_filter']['delete']['1'] = "Filter ID %s löschen";
$GLOBALS['TL_LANG']['tl_led_filter']['copyChildren']['0'] = "Filter mit Unterbegriffen kopieren";
$GLOBALS['TL_LANG']['tl_led_filter']['copyChildren']['1'] = "Filter ID %s mit Unterbegriffen kopieren";
$GLOBALS['TL_LANG']['tl_led_filter']['synchronize'] = "Filter synchronisieren";
