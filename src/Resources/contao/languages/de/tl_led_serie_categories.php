<?php
/**
 * TL_ROOT/system/modules/agape/languages/de/tl_led_serie_categories.php
 *
 * Contao extension: agape
 * Deutsch translation file
 *
 * Copyright  Sven Rhinow Webentwicklung 2017 <http://www.sr-tag.de>
 * Author     Sven Rhinow
 * @license    LGPL-3.0+
 * Translator: Sven Rhinow (sr-tag)
 */

$GLOBALS['TL_LANG']['tl_led_serie_categories']['title'] = "Serien-Kategorie";
$GLOBALS['TL_LANG']['tl_led_serie_categories']['seo_legend'] = "Suchmaschinenoptimierung";
$GLOBALS['TL_LANG']['tl_led_serie_categories']['categories'] = "Kategorien";

/** Legends */
$GLOBALS['TL_LANG']['tl_led_serie_categories']['image_legend'] = "Bild-Einstellungen";
$GLOBALS['TL_LANG']['tl_led_serie_categories']['template_legend'] = "Template-Einstellungen";
$GLOBALS['TL_LANG']['tl_led_serie_categories']['data_legend'] = "Daten";
$GLOBALS['TL_LANG']['tl_led_serie_categories']['extend_legend'] = "weitere Einstellungen";

/* Fields */
$GLOBALS['TL_LANG']['tl_led_serie_categories']['name']['0'] = "Name";
$GLOBALS['TL_LANG']['tl_led_serie_categories']['name']['1'] = "Name der Serien-Kategorie.";
$GLOBALS['TL_LANG']['tl_led_serie_categories']['alias']['0'] = "Alias";
$GLOBALS['TL_LANG']['tl_led_serie_categories']['alias']['1'] = "Alias, um auf die Serien-Kategorie verweisen zu können.";
$GLOBALS['TL_LANG']['tl_led_serie_categories']['addImage']['0'] = "Bild hinzufügen";
$GLOBALS['TL_LANG']['tl_led_serie_categories']['addImage']['1'] = "";
$GLOBALS['TL_LANG']['tl_led_serie_categories']['image']['0'] = "Bild";
$GLOBALS['TL_LANG']['tl_led_serie_categories']['image']['1'] = "Bild auswählen, welches zum Produkt bereit gestellt weden soll.";
$GLOBALS['TL_LANG']['tl_led_serie_categories']['alt']['0'] = "Alternativtext für das Bild";
$GLOBALS['TL_LANG']['tl_led_serie_categories']['alt']['1'] = "wird im Quelltext geschrieben und hilft Brailgeräte und Google ;)";
$GLOBALS['TL_LANG']['tl_led_serie_categories']['teaser']['0'] = "Kurzbeschreibung";
$GLOBALS['TL_LANG']['tl_led_serie_categories']['teaser']['1'] = "Teaser zum Produkt.";
$GLOBALS['TL_LANG']['tl_led_serie_categories']['description']['0'] = "Beschreibung";
$GLOBALS['TL_LANG']['tl_led_serie_categories']['description']['1'] = "Beschreibung zum Produkt.";
$GLOBALS['TL_LANG']['tl_led_serie_categories']['published']['0'] = "online sichtbar";
$GLOBALS['TL_LANG']['tl_led_serie_categories']['published']['1'] = "Haken setzen wenn dieser Eintrag auf der Website angezeigt werden soll.";
$GLOBALS['TL_LANG']['tl_led_serie_categories']['designer']['0'] = "Designer";
$GLOBALS['TL_LANG']['tl_led_serie_categories']['designer']['1'] = "";
$GLOBALS['TL_LANG']['tl_led_serie_categories']['issued_at']['0'] = "erstellt im Jahr";
$GLOBALS['TL_LANG']['tl_led_serie_categories']['serieCategoryListTpl']['0'] = "Kategorie-Template";
$GLOBALS['TL_LANG']['tl_led_serie_categories']['serieCategoryListTpl']['1'] = "Darstellung der Tabellenstrultur für diese Kategorie";
$GLOBALS['TL_LANG']['tl_led_serie_categories']['seo_title']['0'] = "SEO-Titel";
$GLOBALS['TL_LANG']['tl_led_serie_categories']['seo_title']['1'] = "Titel für die Suchmaschienenoptimierung (Quelltext)";
$GLOBALS['TL_LANG']['tl_led_serie_categories']['seo_keywords']['0'] = "SEO-Schlagwörte";
$GLOBALS['TL_LANG']['tl_led_serie_categories']['seo_keywords']['1'] = "Keywords für die Suchmaschienenoptimierung (Quelltext)";
$GLOBALS['TL_LANG']['tl_led_serie_categories']['seo_description']['0'] = "SEO-Beschreibungstext";
$GLOBALS['TL_LANG']['tl_led_serie_categories']['seo_description']['1'] = "Beschreibungstext für die Suchmaschienenoptimierung (Quelltext)";
$GLOBALS['TL_LANG']['tl_led_serie_categories']['article_sorting']['0'] = "Sortierung der Produkte/Artikel dieser Kategorie anpassen";
$GLOBALS['TL_LANG']['tl_led_serie_categories']['article_sorting']['1'] = "z.B. `output_power` ASC, ASC = ascending, DESC = descending, kann auch kombiniert werden z.B. `alias` ASC, `output_power` ASC";
/* Activities */
$GLOBALS['TL_LANG']['tl_led_serie_categories']['new']['0'] = "Neue Serien-Kategorie";
$GLOBALS['TL_LANG']['tl_led_serie_categories']['new']['1'] = "Neue Serien-Kategorie erstellen.";
$GLOBALS['TL_LANG']['tl_led_serie_categories']['show']['0'] = "Einzelheiten der Serien-Kategorie";
$GLOBALS['TL_LANG']['tl_led_serie_categories']['show']['1'] = "Einzelheiten der Serien-Kategorie ID %s anzeigen";
$GLOBALS['TL_LANG']['tl_led_serie_categories']['edit']['0'] = "Serien-Kategorie bearbeiten";
$GLOBALS['TL_LANG']['tl_led_serie_categories']['edit']['1'] = "Serien-Kategorie ID %s bearbeiten";
$GLOBALS['TL_LANG']['tl_led_serie_categories']['copy']['0'] = "Serien-Kategorie kopieren";
$GLOBALS['TL_LANG']['tl_led_serie_categories']['copy']['1'] = "Serien-Kategorie ID %s kopieren";
$GLOBALS['TL_LANG']['tl_led_serie_categories']['cut']['0'] = "Serien-Kategorie verschieben";
$GLOBALS['TL_LANG']['tl_led_serie_categories']['cut']['1'] = "Serien-Kategorie ID %s verschieben";
$GLOBALS['TL_LANG']['tl_led_serie_categories']['delete']['0'] = "Serien-Kategorie löschen";
$GLOBALS['TL_LANG']['tl_led_serie_categories']['delete']['1'] = "Serien-Kategorie ID %s löschen";
$GLOBALS['TL_LANG']['tl_led_serie_categories']['toggle']['0'] = "aktivieren/ deaktivieren";
$GLOBALS['TL_LANG']['tl_led_serie_categories']['toggle']['1'] = "Den Eintrag aktivieren bzw. deaktivieren";
