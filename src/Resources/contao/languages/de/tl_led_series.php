<?php
/**
 * TL_ROOT/vendor/srhinow/ledproducts-bundle/src/Recources/contao/modules/languages/de/tl_led_series.php
 *
 * Contao extension: ledproducts-bundle
 * Deutsch translation file
 *
 * Copyright  Sven Rhinow Webentwicklung 2017 <http://www.sr-tag.de>
 * Author     Sven Rhinow
 * @license    LGPL-3.0+
 * Translator: Sven Rhinow (sr-tag)
 */
$GLOBALS['TL_LANG']['tl_led_series']['title'] = "Serie";
$GLOBALS['TL_LANG']['tl_led_series']['product_categories'] = "Produkt-Kategorien";
$GLOBALS['TL_LANG']['tl_led_series']['product_filter'] = "Produkt-Filter";

/** Legends */

$GLOBALS['TL_LANG']['tl_led_series']['data_legend'] = "Daten";
$GLOBALS['TL_LANG']['tl_led_series']['image_legend'] = "Bild";
$GLOBALS['TL_LANG']['tl_led_series']['template_legend'] = "Templates";
$GLOBALS['TL_LANG']['tl_led_series']['seo_legend'] = "Suchmaschinenoptimierung";
$GLOBALS['TL_LANG']['tl_led_series']['extend_legend'] = "weitere Einstellungen";

/* Fields */
$GLOBALS['TL_LANG']['tl_led_series']['name']['0'] = "Name";
$GLOBALS['TL_LANG']['tl_led_series']['name']['1'] = "Name der Serie.";
$GLOBALS['TL_LANG']['tl_led_series']['alias']['0'] = "Alias";
$GLOBALS['TL_LANG']['tl_led_series']['alias']['1'] = "Alias, um auf der Serie verweisen zu können.";
$GLOBALS['TL_LANG']['tl_led_series']['sorting']['0'] = "Sortierung";
$GLOBALS['TL_LANG']['tl_led_series']['sorting']['1'] = "Danach wird im Frontend und kann im Backend sortiert werden.";
$GLOBALS['TL_LANG']['tl_led_series']['category']['0'] = "Kategorie";
$GLOBALS['TL_LANG']['tl_led_series']['category']['1'] = "die zugehoerige Kategorie auswählen.";
$GLOBALS['TL_LANG']['tl_led_series']['addImage']['0'] = "Bild hinzufügen";
$GLOBALS['TL_LANG']['tl_led_series']['addImage']['1'] = "";
$GLOBALS['TL_LANG']['tl_led_series']['image']['0'] = "Bild";
$GLOBALS['TL_LANG']['tl_led_series']['image']['1'] = "Bild auswählen, welches zur Serie bereit gestellt weden soll.";$GLOBALS['TL_LANG']['tl_led_series']['addTeaserImage']['0'] = "abweichendes Bild für den Teaser hinzufügen";
$GLOBALS['TL_LANG']['tl_led_series']['addTeaserImage']['1'] = "";
$GLOBALS['TL_LANG']['tl_led_series']['teaserImage']['0'] = "TeaserBild";
$GLOBALS['TL_LANG']['tl_led_series']['teaserImage']['1'] = "nur Bild auswählen, wenn in der Teaser-Ansicht der Serie ein anderes bild angezeigt werden soll.";
$GLOBALS['TL_LANG']['tl_led_series']['alt']['0'] = "Alternativtext für das Bild";
$GLOBALS['TL_LANG']['tl_led_series']['alt']['1'] = "wird im Quelltext geschrieben und hilft Brailgeräte und Google ;)";
$GLOBALS['TL_LANG']['tl_led_series']['teaser']['0'] = "Kurzbeschreibung";
$GLOBALS['TL_LANG']['tl_led_series']['teaser']['1'] = "Teaser zur Serie";
$GLOBALS['TL_LANG']['tl_led_series']['description']['0'] = "Beschreibung";
$GLOBALS['TL_LANG']['tl_led_series']['description']['1'] = "Beschreibung zur Serie.";
$GLOBALS['TL_LANG']['tl_led_series']['features']['0'] = "Features";
$GLOBALS['TL_LANG']['tl_led_series']['features']['1'] = "Features com Serien-Artikel.";
$GLOBALS['TL_LANG']['tl_led_series']['serieListTpl']['0'] = "Serien-Listen-Template";
$GLOBALS['TL_LANG']['tl_led_series']['serieListTpl']['1'] = "Falls die Serien-Listenansicht vom Standart-Template abweicht,wähen sie hier das passende aus.";
$GLOBALS['TL_LANG']['tl_led_series']['serieDetailTpl']['0'] = "Serien-Details-Template";
$GLOBALS['TL_LANG']['tl_led_series']['serieDetailTpl']['1'] = "Falls die Serien-Detailansicht vom Standart-Template abweicht,wähen sie hier das passende aus.";
$GLOBALS['TL_LANG']['tl_led_series']['articleListTpl']['0'] = "Artikel-Listen-Template";
$GLOBALS['TL_LANG']['tl_led_series']['articleListTpl']['1'] = "Falls die Artikel-Listenansicht vom Standart-Template abweicht,wähen sie hier das passende aus.";
$GLOBALS['TL_LANG']['tl_led_series']['categoryArticleListTpl']['0'] = "Standart-Kategorie-Template";
$GLOBALS['TL_LANG']['tl_led_series']['categoryArticleListTpl']['1'] = "Hier wird für dieses Seire das Standart Tabellen-Listen-Tempplate .";
$GLOBALS['TL_LANG']['tl_led_series']['articleDetailsTpl']['0'] = "Artikel-Details-Template";
$GLOBALS['TL_LANG']['tl_led_series']['articleDetailsTpl']['1'] = "Falls die Artikel-Detailansicht vom Standart-Template abweicht,wähen sie hier das passende aus.";
$GLOBALS['TL_LANG']['tl_led_series']['clickForBigger']['0'] = "Vergrößerung aktivieren";
$GLOBALS['TL_LANG']['tl_led_series']['clickForBigger']['1'] = "Damit in der Detailansicht die Vergrößerung per Klick möglich ist.";
$GLOBALS['TL_LANG']['tl_led_series']['published']['0'] = "online sichtbar";
$GLOBALS['TL_LANG']['tl_led_series']['published']['1'] = "Haken setzen wenn dieser Eintrag auf der Website angezeigt werden soll.";
$GLOBALS['TL_LANG']['tl_led_series']['set_default_categories']['0'] = "die Standart Kategorien setzen";
$GLOBALS['TL_LANG']['tl_led_series']['set_default_categories']['1'] = "!!! Alle evtl. bereits angelegten Kategorien werden gelöscht. Falls bereits Artikel bestehen, Verknüpfungen sind dann nicht mehr gültig !!!";

$GLOBALS['TL_LANG']['tl_led_series']['seo_title']['0'] = "SEO-Titel";
$GLOBALS['TL_LANG']['tl_led_series']['seo_title']['1'] = "Titel für die Suchmaschienenoptimierung (Quelltext)";
$GLOBALS['TL_LANG']['tl_led_series']['seo_keywords']['0'] = "SEO-Schlagwörte";
$GLOBALS['TL_LANG']['tl_led_series']['seo_keywords']['1'] = "Keywords für die Suchmaschienenoptimierung (Quelltext)";
$GLOBALS['TL_LANG']['tl_led_series']['seo_description']['0'] = "SEO-Beschreibungstext";
$GLOBALS['TL_LANG']['tl_led_series']['seo_description']['1'] = "Beschreibungstext für die Suchmaschienenoptimierung (Quelltext)";

/* Activities */
$GLOBALS['TL_LANG']['tl_led_series']['new']['0'] = "Neue Serie";
$GLOBALS['TL_LANG']['tl_led_series']['new']['1'] = "Neue Serie erstellen.";
$GLOBALS['TL_LANG']['tl_led_series']['show']['0'] = "Einzelheiten der Serie";
$GLOBALS['TL_LANG']['tl_led_series']['show']['1'] = "Einzelheiten der Serie ID %s anzeigen";
$GLOBALS['TL_LANG']['tl_led_series']['edit_article']['0'] = "Artikel der Serie bearbeiten";
$GLOBALS['TL_LANG']['tl_led_series']['edit_article']['1'] = "Artikel der Serie ID %s bearbeiten";
$GLOBALS['TL_LANG']['tl_led_series']['edit']['0'] = "Serie bearbeiten";
$GLOBALS['TL_LANG']['tl_led_series']['edit']['1'] = "Serie ID %s bearbeiten";
$GLOBALS['TL_LANG']['tl_led_series']['copy']['0'] = "Serie kopieren";
$GLOBALS['TL_LANG']['tl_led_series']['copy']['1'] = "Serie ID %s kopieren";
$GLOBALS['TL_LANG']['tl_led_series']['cut']['0'] = "Serie verschieben";
$GLOBALS['TL_LANG']['tl_led_series']['cut']['1'] = "Serie ID %s verschieben";
$GLOBALS['TL_LANG']['tl_led_series']['delete']['0'] = "Serie löschen";
$GLOBALS['TL_LANG']['tl_led_series']['delete']['1'] = "Serie ID %s löschen";
$GLOBALS['TL_LANG']['tl_led_series']['toggle']['0'] = "aktivieren/ deaktivieren";
$GLOBALS['TL_LANG']['tl_led_series']['toggle']['1'] = "Den Eintrag aktivieren bzw. deaktivieren";
$GLOBALS['TL_LANG']['tl_led_series']['serie_categories']['0'] = "Serien-Kategorien";
$GLOBALS['TL_LANG']['tl_led_series']['serie_categories']['1'] = "Kategorien der Serie ID %s verwalten";
