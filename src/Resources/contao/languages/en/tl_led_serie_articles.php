<?php
/**
 * TL_ROOT/vendor/srhinow/ledproducts-bundle/src/Recources/contao/modules/languages/de/tl_led_serie_articles.php
 *
 * Contao extension: ledproducts-bundle
 * Deutsch translation file
 *
 * Copyright  Sven Rhinow Webentwicklung 2017 <http://www.sr-tag.de>
 * Author     Sven Rhinow
 * @license    LGPL-3.0+
 * Translator: Sven Rhinow (sr-tag)
 */

$GLOBALS['TL_LANG']['tl_led_serie_articles']['title'] = "Serien-Artikel";
$GLOBALS['TL_LANG']['tl_led_serie_articles']['seo_legend'] = "Suchmaschinenoptimierung";

/** Legends */
$GLOBALS['TL_LANG']['tl_led_serie_articles']['data_legend'] = "Daten";
$GLOBALS['TL_LANG']['tl_led_serie_articles']['webstore_legend'] = "Webshop-Einstellungen";
$GLOBALS['TL_LANG']['tl_led_serie_articles']['extend_legend'] = "weitere Einstellungen";
$GLOBALS['TL_LANG']['tl_led_serie_articles']['download_legend'] = "Download";
$GLOBALS['TL_LANG']['tl_led_serie_articles']['image_legend'] = "Bild";

/* Fields */
$GLOBALS['TL_LANG']['tl_led_serie_articles']['name']['0'] = "Name";
$GLOBALS['TL_LANG']['tl_led_serie_articles']['name']['1'] = "Der Name des Serien-Artikels.";
$GLOBALS['TL_LANG']['tl_led_serie_articles']['alias']['0'] = "Alias";
$GLOBALS['TL_LANG']['tl_led_serie_articles']['alias']['1'] = "Alias, um auf den Serien-Artikel verweisen zu können.";
$GLOBALS['TL_LANG']['tl_led_serie_articles']['category']['0'] = "Kategorie";
$GLOBALS['TL_LANG']['tl_led_serie_articles']['category']['1'] = "die zugehoerige Kategorie auswählen.";
$GLOBALS['TL_LANG']['tl_led_serie_articles']['addImage']['0'] = "Bild hinzufügen";
$GLOBALS['TL_LANG']['tl_led_serie_articles']['addImage']['1'] = "";
$GLOBALS['TL_LANG']['tl_led_serie_articles']['image']['0'] = "Bild";
$GLOBALS['TL_LANG']['tl_led_serie_articles']['image']['1'] = "Bild auswählen, welches zum Serien-Artikel bereit gestellt weden soll.";
$GLOBALS['TL_LANG']['tl_led_serie_articles']['alt']['0'] = "Alternativtext für das Bild";
$GLOBALS['TL_LANG']['tl_led_serie_articles']['alt']['1'] = "wird im Quelltext geschrieben und hilft Brailgeräte und Google ;)";
$GLOBALS['TL_LANG']['tl_led_serie_articles']['teaser']['0'] = "Kurzbeschreibung";
$GLOBALS['TL_LANG']['tl_led_serie_articles']['teaser']['1'] = "Teaser zum Serien-Artikel.";
$GLOBALS['TL_LANG']['tl_led_serie_articles']['description']['0'] = "Beschreibung";
$GLOBALS['TL_LANG']['tl_led_serie_articles']['description']['1'] = "Beschreibung zum Serien-Artikel.";
$GLOBALS['TL_LANG']['tl_led_serie_articles']['published']['0'] = "online sichtbar";
$GLOBALS['TL_LANG']['tl_led_serie_articles']['published']['1'] = "Haken setzen wenn dieser Eintrag auf der Website angezeigt werden soll.";
$GLOBALS['TL_LANG']['tl_led_serie_articles']['size']['0'] = "size";
$GLOBALS['TL_LANG']['tl_led_serie_articles']['size']['1'] = "";
$GLOBALS['TL_LANG']['tl_led_serie_articles']['viewing_angle']['0'] = "viewing angle (°)";
$GLOBALS['TL_LANG']['tl_led_serie_articles']['viewing_angle']['1'] = "keine Einheit eingeben.";
$GLOBALS['TL_LANG']['tl_led_serie_articles']['circuit_substrate']['0'] = "circuit substrate";
$GLOBALS['TL_LANG']['tl_led_serie_articles']['circuit_substrate']['1'] = "";
$GLOBALS['TL_LANG']['tl_led_serie_articles']['forward_current_typ']['0'] = "forward current (typ) mA";
$GLOBALS['TL_LANG']['tl_led_serie_articles']['forward_current_typ']['1'] = "keine Einheit eingeben.";
$GLOBALS['TL_LANG']['tl_led_serie_articles']['forward_current_max']['0'] = "forward current (max) mA";
$GLOBALS['TL_LANG']['tl_led_serie_articles']['forward_current_max']['1'] = "keine Einheit eingeben.";
$GLOBALS['TL_LANG']['tl_led_serie_articles']['forward_current_pulsed']['0'] = "forward current (pulsed) mA";
$GLOBALS['TL_LANG']['tl_led_serie_articles']['forward_current_pulsed']['1'] = "keine Einheit eingeben.";
$GLOBALS['TL_LANG']['tl_led_serie_articles']['thermal_resistance']['0'] = "thermal resistance (K/W)";
$GLOBALS['TL_LANG']['tl_led_serie_articles']['thermal_resistance']['1'] = "keine Einheit eingeben.";
$GLOBALS['TL_LANG']['tl_led_serie_articles']['color']['0'] = "color";
$GLOBALS['TL_LANG']['tl_led_serie_articles']['color']['1'] = "";
$GLOBALS['TL_LANG']['tl_led_serie_articles']['wavelength_from']['0'] = "wavelength from (nm)";
$GLOBALS['TL_LANG']['tl_led_serie_articles']['wavelength_from']['1'] = "keine Einheit eingeben.";
$GLOBALS['TL_LANG']['tl_led_serie_articles']['wavelength_to']['0'] = "wavelength to (nm)";
$GLOBALS['TL_LANG']['tl_led_serie_articles']['wavelength_to']['1'] = "keine Einheit eingeben.";
$GLOBALS['TL_LANG']['tl_led_serie_articles']['wavelength_type']['0'] = "wavelength type";
//$GLOBALS['TL_LANG']['tl_led_serie_articles']['wavelength_type']['1'] = "keine Einheit eingeben.";
$GLOBALS['TL_LANG']['tl_led_serie_articles']['fwhm']['0'] = "FWHM (nm)";
$GLOBALS['TL_LANG']['tl_led_serie_articles']['fwhm']['1'] = "keine Einheit eingeben.";
$GLOBALS['TL_LANG']['tl_led_serie_articles']['chip_material']['0'] = "chip material";
$GLOBALS['TL_LANG']['tl_led_serie_articles']['chip_material']['1'] = "";
$GLOBALS['TL_LANG']['tl_led_serie_articles']['radiant_intensity']['0'] = "radiant intensity (mW/sr)";
$GLOBALS['TL_LANG']['tl_led_serie_articles']['radiant_intensity']['1'] = "keine Einheit eingeben.";
$GLOBALS['TL_LANG']['tl_led_serie_articles']['luminous_intensity']['0'] = "luminous intensity";
$GLOBALS['TL_LANG']['tl_led_serie_articles']['luminous_intensity']['1'] = "keine Einheit eingeben.";
$GLOBALS['TL_LANG']['tl_led_serie_articles']['output_power']['0'] = "output power (mW)";
$GLOBALS['TL_LANG']['tl_led_serie_articles']['output_power']['1'] = "keine Einheit eingeben.";
$GLOBALS['TL_LANG']['tl_led_serie_articles']['datasheet']['0'] = "technical data sheet";
$GLOBALS['TL_LANG']['tl_led_serie_articles']['datasheet']['1'] = "";
$GLOBALS['TL_LANG']['tl_led_serie_articles']['rayfile']['0'] = "ray file";
$GLOBALS['TL_LANG']['tl_led_serie_articles']['rayfile']['1'] = "";
$GLOBALS['TL_LANG']['tl_led_serie_articles']['reach_rohs']['0'] = "REACH / RoHS";
$GLOBALS['TL_LANG']['tl_led_serie_articles']['reach_rohs']['1'] = "";

$GLOBALS['TL_LANG']['tl_led_serie_articles']['webstore_germany'] = ['Webstores in Germany'];
$GLOBALS['TL_LANG']['tl_led_serie_articles']['webstore_global'] = ['globale Webstores'];
$GLOBALS['TL_LANG']['tl_led_serie_articles']['webstore'] = ['Webstore'];
$GLOBALS['TL_LANG']['tl_led_serie_articles']['webstore_url'] = ['Webstore-Url'];


$GLOBALS['TL_LANG']['tl_led_serie_articles']['seo_title']['0'] = "SEO-Titel";
$GLOBALS['TL_LANG']['tl_led_serie_articles']['seo_title']['1'] = "Titel für die Suchmaschienenoptimierung (Quelltext)";
$GLOBALS['TL_LANG']['tl_led_serie_articles']['seo_keywords']['0'] = "SEO-Schlagwörte";
$GLOBALS['TL_LANG']['tl_led_serie_articles']['seo_keywords']['1'] = "Keywords für die Suchmaschienenoptimierung (Quelltext)";
$GLOBALS['TL_LANG']['tl_led_serie_articles']['seo_description']['0'] = "SEO-Beschreibungstext";
$GLOBALS['TL_LANG']['tl_led_serie_articles']['seo_description']['1'] = "Beschreibungstext für die Suchmaschienenoptimierung (Quelltext)";

/* Activities */
$GLOBALS['TL_LANG']['tl_led_serie_articles']['new']['0'] = "Neuer Serien-Artikel";
$GLOBALS['TL_LANG']['tl_led_serie_articles']['new']['1'] = "Neuen Serien-Artikel erstellen.";
$GLOBALS['TL_LANG']['tl_led_serie_articles']['show']['0'] = "Einzelheiten vom Serien-Artikel";
$GLOBALS['TL_LANG']['tl_led_serie_articles']['show']['1'] = "Einzelheiten vom Serien-Artikel ID %s anzeigen";
$GLOBALS['TL_LANG']['tl_led_serie_articles']['edit']['0'] = "Serien-Artikel bearbeiten";
$GLOBALS['TL_LANG']['tl_led_serie_articles']['edit']['1'] = "Serien-Artikel ID %s bearbeiten";
$GLOBALS['TL_LANG']['tl_led_serie_articles']['copy']['0'] = "Serien-Artikel kopieren";
$GLOBALS['TL_LANG']['tl_led_serie_articles']['copy']['1'] = "Serien-Artikel ID %s kopieren";
$GLOBALS['TL_LANG']['tl_led_serie_articles']['cut']['0'] = "Serien-Artikel verschieben";
$GLOBALS['TL_LANG']['tl_led_serie_articles']['cut']['1'] = "Serien-Artikel ID %s verschieben";
$GLOBALS['TL_LANG']['tl_led_serie_articles']['delete']['0'] = "Serien-Artikel löschen";
$GLOBALS['TL_LANG']['tl_led_serie_articles']['delete']['1'] = "Serien-Artikel ID %s löschen";
$GLOBALS['TL_LANG']['tl_led_serie_articles']['toggle']['0'] = "aktivieren/ deaktivieren";
$GLOBALS['TL_LANG']['tl_led_serie_articles']['toggle']['1'] = "Den Eintrag aktivieren bzw. deaktivieren";

//Options
$GLOBALS['TL_LANG']['tl_led_serie_articles']['webstore_germany_options'] = [
    'conrad' => 'Conrad',
    'farnell' => 'Farnell',
    'rs_components' => 'RS Components',
    'mercateo' => 'Mercateo',
    'digitalo' => 'digitalo',
    'voelkner' => 'Voelkner'
    ];

$GLOBALS['TL_LANG']['tl_led_serie_articles']['webstore_global_options'] = [
    'conrad' => 'Conrad',
    'farnell' => 'Farnell',
    'rs_components' => 'RS Components',
    'newark' => 'Newark'
];

$GLOBALS['TL_LANG']['tl_led_serie_articles']['wavelength_type_options'] = [
    'dominant_wavelength'=>'dominant Wavelength',
    'peak_wavelength'=>'Peak-Wavelength',
    'centroid_wavelength'=>'Centroid-Wavelength'
];