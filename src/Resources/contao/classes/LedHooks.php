<?php
/**
 * @copyright  Sven Rhinow Webentwicklung 2017 <http://www.sr-tag.de>
 * @author     Sven Rhinow
 * @package    led
 * @license    http://www.gnu.org/licenses/lgpl-3.0.html LGPL
 * @filesource
 */

namespace Srhinow\LedproductsBundle;

class LedHooks extends \Frontend
{
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * replace ledproducts-bundle-specific inserttag if get-paramter isset
     * led::category|product::colname from objPage
     * @param string $strTag
     * @return string
     */
    public function ledReplaceInsertTags($strTag)
    {
        if (substr($strTag,0,7) == 'led::')
        {
//            global $objPage;

            $split = explode('::',$strTag);

            switch($split[1]){
                case 'category':
                    $categoryId = \Input::get('category');
                    $objCategory = LedCategoriesModel::findByIdOrAlias($categoryId);
                    if($objCategory != null) return $objCategory->$split[2];

                    break;
                case 'serie':
                    $productId = \Input::get('serie');
                    $objSerie = LedSeriesModel::findByIdOrAlias($productId);
                    if($objSerie != null) return $objSerie->$split[2];
            }

        }

        return false;
    }

    /**
     * passt die Breadcrumb-Navi den Category -> Produktliste -> Produktdetails mit den jeweiligen Namen an
     * @param $arrItems
     * @param $objModule
     * @return mixed
     */
    public function LedGenerateBreadcrumb($arrItems, $objModule)
    {
        $count = count($arrItems) -1;
        if(\Input::get('category')) {
            $objCategory = LedCategoriesModel::findByIdOrAlias(\Input::get('category'));
            if($objCategory != null) {
                $arrItems[$count]['title'] = $objCategory->name;
                $arrItems[$count]['link'] = $objCategory->name;
            }
        }
        if(\Input::get('serie')) {

            $objSerie = LedSeriesModel::findByIdOrAlias(\Input::get('serie'));
            if($objSerie != null) {
                //Produktbereich anpassen
                $arrItems[$count]['title'] = $objSerie->name;
                $arrItems[$count]['link'] = $objSerie->name;

                //Kategorielink anpassen
                $catCount = $count -1;
                $catPageId = $arrItems[$catCount]['data']['id'];

                $objCategory = LedCategoriesModel::findById($objSerie->pid);
                $pageObj = \PageModel::findById($catPageId);

                if($pageObj != null && $objCategory != null) {
                    $newPageUrl = $pageObj->getFrontendUrl(((\Config::get('useAutoItem') && !\Config::get('disableAlias')) ? '/' : '/items/') . ((!\Config::get('disableAlias') && $objCategory->alias != '') ? $objCategory->alias : $objCategory->id));
                    $arrItems[$catCount]['href'] = $newPageUrl;
                    $arrItems[$catCount]['title'] = $objCategory->name;
                    $arrItems[$catCount]['link'] = $objCategory->name;
                }
            }
        }
        if(\Input::get('article')) {

            $objArticle = LedSerieArticlesModel::findByIdOrAlias(\Input::get('article'));
            if($objArticle != null) {
                //Articlebereich anpassen
                $arrItems[$count]['title'] = $objArticle->name;
                $arrItems[$count]['link'] = $objArticle->name;

                //Serienlink anpassen
                $serieCount = $count -1;
                $seriePageId = $arrItems[$serieCount]['data']['id'];

                $objSerie = LedSeriesModel::findByIdOrAlias($objArticle->pid);
                $seriePageObj = \PageModel::findById($seriePageId);

                if($seriePageObj != null && $objSerie != null) {

                    $seriePageUrl = $seriePageObj->getFrontendUrl(((\Config::get('useAutoItem') && !\Config::get('disableAlias')) ? '/' : '/items/') . ((!\Config::get('disableAlias') && $objSerie->alias != '') ? $objSerie->alias : $objSerie->id));
                    $arrItems[$serieCount]['href'] = $seriePageUrl;
                    $arrItems[$serieCount]['title'] = $objSerie->name;
                    $arrItems[$serieCount]['link'] = $objSerie->name;

                    //Kategorielink anpassen
                    $catCount = $serieCount -1;
                    $catPageId = $arrItems[$catCount]['data']['id'];

                    $objCategory = LedCategoriesModel::findByIdOrAlias($objSerie->pid);
                    $catPageObj = \PageModel::findById($catPageId);

                    if($catPageObj != null && $objCategory != null) {

                        $catPageUrl = $catPageObj->getFrontendUrl(((\Config::get('useAutoItem') && !\Config::get('disableAlias')) ? '/' : '/items/') . ((!\Config::get('disableAlias') && $objCategory->alias != '') ? $objCategory->alias : $objCategory->id));
                        $arrItems[$catCount]['href'] = $catPageUrl;
                        $arrItems[$catCount]['title'] = $objCategory->name;
                        $arrItems[$catCount]['link'] = $objCategory->name;
                    }
                }
            }
        }

        return $arrItems;
    }
}
