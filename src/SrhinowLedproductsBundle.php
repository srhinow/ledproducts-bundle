<?php

/**
 * @copyright  Sven Rhinow <https://www.sr-tag.de>
 * @author     Sven Rhinow
 * @package    LedproductsBundle
 * @license    LGPL-3.0+
 * @see	       https://github.com/srhinow/ledproducts-bundle
 *
 */

namespace Srhinow\LedproductsBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * Configures the Contao teaser-manager bundle.
 *
 * @author Sven Rhinow
 */
class SrhinowLedproductsBundle extends Bundle
{
}
